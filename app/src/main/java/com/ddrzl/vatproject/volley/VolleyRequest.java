package com.ddrzl.vatproject.volley;

import android.content.Context;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class VolleyRequest {
    public void postJSONObjectRequest(Context mContext, String url, JSONObject jsonValue, final VolleyCallback callback) {
        if (jsonValue == null) {
            jsonValue = new JSONObject();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, jsonValue,
                (JSONObject result) -> callback.onSuccessResponse(result.toString()),
                callback::onErrorResponse) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<>();
                params.put("x-api-key", "CODEX@123");
                params.put("Authorization", "Basic YWRtaW46MTIzNA==");
                return params;
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(60 * 100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MyVolleySingleton.getInstance(mContext).addToRequestQueue(jsonObjectRequest);
    }
}
