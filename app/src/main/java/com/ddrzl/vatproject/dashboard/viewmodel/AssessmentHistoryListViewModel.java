package com.ddrzl.vatproject.dashboard.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import com.ddrzl.vatproject.dashboard.entity.NetworkScanningAssessment;
import com.ddrzl.vatproject.dashboard.entity.WebScanningAssessment;
import com.ddrzl.vatproject.dashboard.service.AssessmentHistoryListService;

import java.util.List;

public class AssessmentHistoryListViewModel extends AndroidViewModel {
    private int user_id;
    private LiveData<List<NetworkScanningAssessment>> networkListLivedata;
    private LiveData<List<WebScanningAssessment>> webListLiveData;

    public AssessmentHistoryListViewModel(@NonNull Application application, int user_id) {
        super(application);
        this.user_id = user_id;
    }

    public LiveData<List<NetworkScanningAssessment>> getHistoryList() {
        if (networkListLivedata == null) {
            loadData();
        }
        return networkListLivedata;
    }

    public LiveData<List<WebScanningAssessment>> getWebHistoryList() {
        if (webListLiveData == null) {
            loadDataWebServer();
        }
        return webListLiveData;
    }


    private void loadData() {
        networkListLivedata = AssessmentHistoryListService.getService(getApplication()).getHistoryList(user_id);
    }

    private void loadDataWebServer() {
        webListLiveData = AssessmentHistoryListService.getService(getApplication()).getWebHistoryList(user_id);
    }

    public static class Factory extends ViewModelProvider.NewInstanceFactory {
        private Application mApplication;
        private int user_id;

        public Factory(Application application, int user_id) {
            mApplication = application;
            this.user_id = user_id;
        }

        @NonNull
        @Override
        public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
            //noinspection unchecked
            return (T) new AssessmentHistoryListViewModel(mApplication, user_id);
        }
    }
}
