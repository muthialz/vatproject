package com.ddrzl.vatproject.dashboard.entity;

public class NetworkScanningAssessment {
    private String assessment_name;
    private int output_nmap_id;
    private int user_id;
    private int target_id;
    private String ip_address;
    private String hostname;
    private String Date;
    private String nmap_id;
    private String protocol;
    private String portid;
    private String state;
    private String service;
    private String s_product;
    private String s_version;
    private String s_info;
    private String s_tunnel;
    private String s_method;
    private String s_conf;

    public String getAssessment_name() {
        return assessment_name;
    }

    public void setAssessment_name(String assessment_name) {
        this.assessment_name = assessment_name;
    }

    public int getOutput_nmap_id() {
        return output_nmap_id;
    }

    public void setOutput_nmap_id(int output_nmap_id) {
        this.output_nmap_id = output_nmap_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        this.Date = date;
    }

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public String getIp_address() {
        return ip_address;
    }

    public void setIp_address(String ip_address) {
        this.ip_address = ip_address;
    }

    public String getNmap_id() {
        return nmap_id;
    }

    public void setNmap_id(String nmap_id) {
        this.nmap_id = nmap_id;
    }

    public String getPortid() {
        return portid;
    }

    public void setPortid(String portid) {
        this.portid = portid;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public String getS_conf() {
        return s_conf;
    }

    public void setS_conf(String s_conf) {
        this.s_conf = s_conf;
    }

    public String getS_info() {
        return s_info;
    }

    public void setS_info(String s_info) {
        this.s_info = s_info;
    }

    public String getS_method() {
        return s_method;
    }

    public void setS_method(String s_method) {
        this.s_method = s_method;
    }

    public String getS_product() {
        return s_product;
    }

    public void setS_product(String s_product) {
        this.s_product = s_product;
    }

    public String getS_tunnel() {
        return s_tunnel;
    }

    public void setS_tunnel(String s_tunnel) {
        this.s_tunnel = s_tunnel;
    }

    public String getS_version() {
        return s_version;
    }

    public void setS_version(String s_version) {
        this.s_version = s_version;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public int getTarget_id() {
        return target_id;
    }

    public void setTarget_id(int target_id) {
        this.target_id = target_id;
    }
}
