package com.ddrzl.vatproject.dashboard.fragment;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ddrzl.vatproject.R;
import com.ddrzl.vatproject.dashboard.adapter.NiktoHistoryAdapter;
import com.ddrzl.vatproject.dashboard.adapter.NmapHistoryAdapter;
import com.ddrzl.vatproject.dashboard.viewmodel.AssessmentHistoryListViewModel;
import com.ddrzl.vatproject.databinding.FragmentDashboardBinding;
import com.ddrzl.vatproject.login.entity.User;
import com.ddrzl.vatproject.utils.UserManager;

import java.util.Objects;

public class DashboardFragment extends Fragment {
    private FragmentDashboardBinding binding;
    private User user;
    private NmapHistoryAdapter adapter;
    private NiktoHistoryAdapter niktoAdapter;
    private AssessmentHistoryListViewModel viewModel;

    public DashboardFragment() {
        // Required empty public constructor
    }

    public static DashboardFragment newInstance() {
        DashboardFragment fragment = new DashboardFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        user = UserManager.getInstance(getContext()).getUser();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_dashboard, container, false);
//        binding.setIsLoading(true);
        binding.loading.setVisibility(View.VISIBLE);

        binding.lstHistory.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.lstHistory.setItemAnimator(new DefaultItemAnimator());
        adapter = new NmapHistoryAdapter();
        binding.lstHistory.setAdapter(adapter);

        binding.lstHistory2.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.lstHistory2.setItemAnimator(new DefaultItemAnimator());
        niktoAdapter = new NiktoHistoryAdapter();
        binding.lstHistory2.setAdapter(niktoAdapter);

        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        AssessmentHistoryListViewModel.Factory factory = new AssessmentHistoryListViewModel.Factory(
                Objects.requireNonNull(getActivity()).getApplication(), user.getUser_id());
        viewModel = ViewModelProviders.of(this, factory).get(AssessmentHistoryListViewModel.class);
        observeViewModel(viewModel);
    }

    public void observeViewModel(AssessmentHistoryListViewModel viewModel) {
        viewModel.getHistoryList().observe(this, network_scanning_histories -> {
            if (network_scanning_histories != null) {
                binding.loading.setVisibility(View.GONE);
                adapter.setNetwork_scanning_histories(network_scanning_histories);
                adapter.notifyDataSetChanged();
            } else {
                binding.loading.setVisibility(View.GONE);
                binding.loading.setText(getString(R.string.no_history_list));
            }
        });

        viewModel.getWebHistoryList().observe(this, web_scanning_histories -> {
            if (web_scanning_histories != null) {
                binding.loading.setVisibility(View.GONE);
                niktoAdapter.setWeb_scanning_histories(web_scanning_histories);
                niktoAdapter.notifyDataSetChanged();
            } else {
                binding.loading.setVisibility(View.GONE);
                binding.loading.setText(getString(R.string.no_history_list));
            }
        });
    }
}
