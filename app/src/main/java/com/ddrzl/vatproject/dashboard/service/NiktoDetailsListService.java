package com.ddrzl.vatproject.dashboard.service;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.ddrzl.vatproject.R;
import com.ddrzl.vatproject.dashboard.entity.WebScanningAssessment;
import com.ddrzl.vatproject.utils.JSONUtil;
import com.ddrzl.vatproject.volley.VolleyCallback;
import com.ddrzl.vatproject.volley.VolleyRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import static com.ddrzl.vatproject.utils.UrlUtil.BASE_URL;
import static com.ddrzl.vatproject.utils.UrlUtil.URL_GET_NIKTO_DETAILS;

public class NiktoDetailsListService {
    private static NiktoDetailsListService niktoDetailsListService;
    private MutableLiveData<List<WebScanningAssessment>> niktoListLiveData;
    private WeakReference<Context> mContextRef;
    private VolleyRequest request = new VolleyRequest();
    private JSONUtil jsonUtil = new JSONUtil();

    private NiktoDetailsListService(Context context) {
        mContextRef = new WeakReference<>(context);
        niktoListLiveData = new MutableLiveData<>();
    }

    public synchronized static NiktoDetailsListService getService(Context context) {
        if (niktoDetailsListService == null) {
            niktoDetailsListService = new NiktoDetailsListService(context);
        }
        return niktoDetailsListService;
    }

    public LiveData<List<WebScanningAssessment>> getNiktoDetails(int target_id, int output_nikto_id) {
        String url = BASE_URL + URL_GET_NIKTO_DETAILS;
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("target_id", target_id);
            jsonObject.put("output_nikto_id", output_nikto_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        request.postJSONObjectRequest(mContextRef.get(), url, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccessResponse(String result) {
                List<WebScanningAssessment> historyList = new ArrayList<>();
                try {
                    JSONObject jsonResult = new JSONObject(result);
                    if (jsonResult.getBoolean("status")) {
                        JSONArray jsonArray = jsonResult.getJSONArray("Report");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            WebScanningAssessment details = new WebScanningAssessment();
                            jsonUtil.toObj(details, jsonArray.getJSONObject(i));
                            historyList.add(details);
                        }
                        niktoListLiveData.setValue(historyList);
                    } else {
                        Toast.makeText(mContextRef.get(), R.string.failed_to_get_assessment_details, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                niktoListLiveData.setValue(historyList);
            }

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(mContextRef.get(), R.string.failed_to_get_assessment_details, Toast.LENGTH_LONG).show();
                Log.e("ERROR", "Error occurred ", error);
            }
        });

        return niktoListLiveData;
    }
}
