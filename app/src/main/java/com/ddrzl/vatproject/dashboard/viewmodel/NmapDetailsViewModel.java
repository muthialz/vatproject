package com.ddrzl.vatproject.dashboard.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import com.ddrzl.vatproject.dashboard.entity.NetworkScanningAssessment;
import com.ddrzl.vatproject.dashboard.service.NmapDetailsListService;

import java.util.List;

public class NmapDetailsViewModel extends AndroidViewModel {
    private int target_id, output_nmap_id;
    private LiveData<List<NetworkScanningAssessment>> nmapListDetails;

    public NmapDetailsViewModel(@NonNull Application application, int target_id, int output_nmap_id) {
        super(application);
        this.target_id = target_id;
        this.output_nmap_id = output_nmap_id;
    }

    public LiveData<List<NetworkScanningAssessment>> getNmapListDetails() {
        if (nmapListDetails == null) {
            loadData();
        }
        return nmapListDetails;
    }

    private void loadData() {
        nmapListDetails = NmapDetailsListService.getService(getApplication()).getNmapDetails(target_id, output_nmap_id);
    }

    public static class Factory extends ViewModelProvider.NewInstanceFactory {
        private Application mApplication;
        private int target_id, output_nmap_id;

        public Factory(Application application, int target_id, int output_nmap_id) {
            mApplication = application;
            this.target_id = target_id;
            this.output_nmap_id = output_nmap_id;
        }

        @NonNull
        @Override
        public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
            //noinspection unchecked
            return (T) new NmapDetailsViewModel(mApplication, target_id, output_nmap_id);
        }
    }
}
