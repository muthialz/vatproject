package com.ddrzl.vatproject.dashboard.service;

import android.app.AlertDialog;
import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.ddrzl.vatproject.R;
import com.ddrzl.vatproject.dashboard.entity.NetworkScanningAssessment;
import com.ddrzl.vatproject.dashboard.entity.WebScanningAssessment;
import com.ddrzl.vatproject.dashboard.fragment.NiktoDetailsFragment;
import com.ddrzl.vatproject.dashboard.fragment.NmapDetailsFragment;
import com.ddrzl.vatproject.login.entity.User;
import com.ddrzl.vatproject.utils.FragmentUtil;
import com.ddrzl.vatproject.utils.JSONUtil;
import com.ddrzl.vatproject.utils.UserManager;
import com.ddrzl.vatproject.volley.VolleyCallback;
import com.ddrzl.vatproject.volley.VolleyRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import dmax.dialog.SpotsDialog;

import static com.ddrzl.vatproject.utils.ConstantUtil.NETWORK_SCANNING;
import static com.ddrzl.vatproject.utils.ConstantUtil.WEB_SCANNING;
import static com.ddrzl.vatproject.utils.UrlUtil.BASE_URL;
import static com.ddrzl.vatproject.utils.UrlUtil.URL_CREATE_ASSESSMENT;
import static com.ddrzl.vatproject.utils.UrlUtil.URL_GET_NIKTO_DETAILS;
import static com.ddrzl.vatproject.utils.UrlUtil.URL_GET_NMAP_DETAILS;
import static com.ddrzl.vatproject.utils.UrlUtil.URL_SCAN_NIKTO;
import static com.ddrzl.vatproject.utils.UrlUtil.URL_SCAN_NMAP;
import static com.ddrzl.vatproject.utils.UrlUtil.URL_UPDATE_ASSESSMENT;

public class AssessmentService {
    private VolleyRequest request = new VolleyRequest();
    private JSONUtil jsonUtil = new JSONUtil();
    private User user;
    private AlertDialog loading_dialog;

    public void createNewAssessment(Context context, String assessment_name, String target, int tool_id) {
        VolleyRequest request = new VolleyRequest();
        String url = BASE_URL + URL_CREATE_ASSESSMENT;
        user = UserManager.getInstance(context).getUser();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("user_id", user.getUser_id());
            jsonObject.put("tool_id", tool_id);
            jsonObject.put("assessment_name", assessment_name);
            if (tool_id == NETWORK_SCANNING) {
                jsonObject.put("target_ip_address", target);
            }
            if (tool_id == WEB_SCANNING) {
                jsonObject.put("target_url", target);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        request.postJSONObjectRequest(context, url, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccessResponse(String result) {

                try {
                    JSONObject jsonResult = new JSONObject(result);
                    if (jsonResult.getBoolean("status")) {
                        Toast.makeText(context, R.string.success_create_assignment, Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(context, R.string.failed_create_assessment, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context, R.string.failed_create_assessment, Toast.LENGTH_LONG).show();
                Log.e("ERROR", "Error occurred ", error);
            }
        });
    }

    public void updateAssessment(Context context, boolean update_1, int target_id, String target, String assessment_name) {
        String url = BASE_URL + URL_UPDATE_ASSESSMENT;
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("target_id", target_id);
            jsonObject.put("assessment_name", assessment_name);
            if (update_1) {
                jsonObject.put("target_ip_address", target);
            } else {
                jsonObject.put("target_url", target);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        request.postJSONObjectRequest(context, url, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccessResponse(String result) {
                try {
                    JSONObject jsonResult = new JSONObject(result);
                    if (jsonResult.getBoolean("status")) {
                        Toast.makeText(context, R.string.assessment_update, Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(context, R.string.failed_update_assessment, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context, R.string.failed_update_assessment, Toast.LENGTH_LONG).show();
                error.printStackTrace();
            }
        });
    }

    public void getNmapDetails(Context context, int target_id, int output_nmap_id) {
        loading_dialog = new SpotsDialog(context, R.style.loading_progress);
        loading_dialog.show();
        String url = BASE_URL + URL_GET_NMAP_DETAILS;
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("target_id", target_id);
            jsonObject.put("output_nmap_id", output_nmap_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        request.postJSONObjectRequest(context, url, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccessResponse(String result) {
                List<NetworkScanningAssessment> nmapList = new ArrayList<>();
                try {
                    JSONObject jsonResult = new JSONObject(result);
                    loading_dialog.dismiss();
                    if (jsonResult.getBoolean("status")) {
                        JSONArray jsonArray = jsonResult.getJSONArray("Report");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            NetworkScanningAssessment details = new NetworkScanningAssessment();
                            jsonUtil.toObj(details, jsonArray.getJSONObject(i));
                            nmapList.add(details);
                        }
                        android.support.v4.app.Fragment fragment = NmapDetailsFragment.newInstance(target_id, output_nmap_id);
                        FragmentUtil.loadFragment((FragmentActivity) context, fragment, true);
                    } else {
                        Toast.makeText(context, R.string.failed_to_get_assessment_details, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorResponse(VolleyError error) {
                loading_dialog.dismiss();
                Toast.makeText(context, R.string.failed_to_get_assessment_details, Toast.LENGTH_LONG).show();
                Log.e("ERROR", "Error occurred ", error);
            }
        });
    }

    public void getNiktoDetails(Context context, int target_id, int output_nikto_id) {
        loading_dialog = new SpotsDialog(context, R.style.loading_progress);
        loading_dialog.show();
        String url = BASE_URL + URL_GET_NIKTO_DETAILS;
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("target_id", target_id);
            jsonObject.put("output_nikto_id", output_nikto_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        request.postJSONObjectRequest(context, url, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccessResponse(String result) {
                List<WebScanningAssessment> niktoList = new ArrayList<>();
                try {
                    JSONObject jsonResult = new JSONObject(result);
                    loading_dialog.dismiss();
                    if (jsonResult.getBoolean("status")) {
                        JSONArray jsonArray = jsonResult.getJSONArray("Report");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            WebScanningAssessment details = new WebScanningAssessment();
                            jsonUtil.toObj(details, jsonArray.getJSONObject(i));
                            niktoList.add(details);
                        }
                        android.support.v4.app.Fragment fragment = NiktoDetailsFragment.newInstance(target_id, output_nikto_id, niktoList);
                        FragmentUtil.loadFragment((FragmentActivity) context, fragment, true);
                    } else {
                        Toast.makeText(context, R.string.failed_to_get_assessment_details, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorResponse(VolleyError error) {
                loading_dialog.dismiss();
                Toast.makeText(context, R.string.failed_to_get_assessment_details, Toast.LENGTH_LONG).show();
                Log.e("ERROR", "Error occurred ", error);
            }
        });
    }

    public void networkScanning(Context context, int target_id, int user_id) {
//        Toast.makeText(context, "Please wait while we scan your target in the background", Toast.LENGTH_LONG).show();
        loading_dialog = new SpotsDialog(context, R.style.scanning_progress);
        loading_dialog.show();
        String url = BASE_URL + URL_SCAN_NMAP;
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("target_id", target_id);
            jsonObject.put("user_id", user_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        request.postJSONObjectRequest(context, url, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccessResponse(String result) {
                List<NetworkScanningAssessment> nmapList = new ArrayList<>();
                try {
                    JSONObject jsonResult = new JSONObject(result);
                    loading_dialog.dismiss();
                    if (jsonResult.getBoolean("status")) {
                        JSONArray jsonArray = jsonResult.getJSONArray("Scan Result");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            NetworkScanningAssessment details = new NetworkScanningAssessment();
                            jsonUtil.toObj(details, jsonArray.getJSONObject(i));
                            nmapList.add(details);
                        }
                        android.support.v4.app.Fragment fragment = NmapDetailsFragment.newInstance(target_id, nmapList.get(0).getOutput_nmap_id());
                        FragmentUtil.loadFragment((FragmentActivity) context, fragment, true);
                    } else {
                        Toast.makeText(context, R.string.failed_to_get_assessment_details, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorResponse(VolleyError error) {
                loading_dialog.dismiss();
                Toast.makeText(context, R.string.failed_to_get_assessment_details, Toast.LENGTH_LONG).show();
                Log.e("ERROR", "Error occurred ", error);
            }
        });
    }

    public void webScanning(Context context, int target_id, int user_id) {
        loading_dialog = new SpotsDialog(context, R.style.scanning_progress);
        loading_dialog.show();
        String url = BASE_URL + URL_SCAN_NIKTO;
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("target_id", target_id);
            jsonObject.put("user_id", user_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        request.postJSONObjectRequest(context, url, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccessResponse(String result) {
                List<WebScanningAssessment> niktoList = new ArrayList<>();
                try {
                    JSONObject jsonResult = new JSONObject(result);
                    loading_dialog.dismiss();
                    if (jsonResult.getBoolean("status")) {
                        JSONArray jsonArray = jsonResult.getJSONArray("scan result");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            WebScanningAssessment details = new WebScanningAssessment();
                            jsonUtil.toObj(details, jsonArray.getJSONObject(i));
                            niktoList.add(details);
                        }
                        android.support.v4.app.Fragment fragment = NiktoDetailsFragment.newInstance(target_id, niktoList.get(0).getOutput_nikto_id(), niktoList);
                        FragmentUtil.loadFragment((FragmentActivity) context, fragment, true);
                    } else {
                        Toast.makeText(context, R.string.failed_to_get_assessment_details, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorResponse(VolleyError error) {
                loading_dialog.dismiss();
                Toast.makeText(context, R.string.failed_to_get_assessment_details, Toast.LENGTH_LONG).show();
                Log.e("ERROR", "Error occurred ", error);
            }
        });
    }
}
