package com.ddrzl.vatproject.dashboard.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import com.ddrzl.vatproject.dashboard.entity.Target;
import com.ddrzl.vatproject.dashboard.service.AssessmentListService;

import java.util.List;

public class AssessmentListViewModel extends AndroidViewModel {
    private int user_id;
    private LiveData<List<Target>> targetLiveData;

    public AssessmentListViewModel(@NonNull Application application, int user_id) {
        super(application);
        this.user_id = user_id;
    }

    public LiveData<List<Target>> getTargetLiveData() {
        if (targetLiveData == null) {
            loadData();
        }
        return targetLiveData;
    }

    private void loadData() {
        targetLiveData = AssessmentListService.getService(getApplication()).getTargetList(user_id);
    }

    public static class Factory extends ViewModelProvider.NewInstanceFactory {
        private Application mApplication;
        private int user_id;

        public Factory(Application application, int user_id) {
            mApplication = application;
            this.user_id = user_id;
        }

        @NonNull
        @Override
        public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
            //noinspection unchecked
            return (T) new AssessmentListViewModel(mApplication, user_id);
        }
    }
}
