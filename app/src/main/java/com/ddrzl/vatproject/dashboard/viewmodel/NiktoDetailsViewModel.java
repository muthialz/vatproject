package com.ddrzl.vatproject.dashboard.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import com.ddrzl.vatproject.dashboard.entity.WebScanningAssessment;
import com.ddrzl.vatproject.dashboard.service.NiktoDetailsListService;

import java.util.List;

public class NiktoDetailsViewModel extends AndroidViewModel {
    private int target_id, output_nikto_id;
    private LiveData<List<WebScanningAssessment>> assessmentList;

    public NiktoDetailsViewModel(@NonNull Application application, int target_id, int output_nikto_id) {
        super(application);
        this.target_id = target_id;
        this.output_nikto_id = output_nikto_id;
    }

    public LiveData<List<WebScanningAssessment>> getAssessmentList() {
        if (assessmentList == null) {
            loadData();
        }
        return assessmentList;
    }

    private void loadData() {
        assessmentList = NiktoDetailsListService.getService(getApplication()).getNiktoDetails(target_id, output_nikto_id);
    }

    public static class Factory extends ViewModelProvider.NewInstanceFactory {
        private Application mApplication;
        private int target_id, output_nikto_id;

        public Factory(Application application, int target_id, int output_nikto_id) {
            mApplication = application;
            this.target_id = target_id;
            this.output_nikto_id = output_nikto_id;
        }

        @NonNull
        @Override
        public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
            //noinspection unchecked
            return (T) new NiktoDetailsViewModel(mApplication, target_id, output_nikto_id);
        }
    }
}
