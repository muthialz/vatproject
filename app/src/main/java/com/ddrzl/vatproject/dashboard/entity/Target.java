package com.ddrzl.vatproject.dashboard.entity;

public class Target {
    private int target_id;
    private String assessment_name;
    private String target_ip_address;
    private String target_url;
    private int tool_id;
    private int user_id;
    private String input_timestamp;

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getTarget_id() {
        return target_id;
    }

    public void setTarget_id(int target_id) {
        this.target_id = target_id;
    }

    public String getAssessment_name() {
        return assessment_name;
    }

    public void setAssessment_name(String assessment_name) {
        this.assessment_name = assessment_name;
    }

    public String getTarget_ip_address() {
        return target_ip_address;
    }

    public void setTarget_ip_address(String target_ip_address) {
        this.target_ip_address = target_ip_address;
    }

    public String getTarget_url() {
        return target_url;
    }

    public void setTarget_url(String target_url) {
        this.target_url = target_url;
    }

    public int getTool_id() {
        return tool_id;
    }

    public void setTool_id(int tool_id) {
        this.tool_id = tool_id;
    }

    public String getInput_timestamp() {
        return input_timestamp;
    }

    public void setInput_timestamp(String input_timestamp) {
        this.input_timestamp = input_timestamp;
    }
}
