package com.ddrzl.vatproject.dashboard.entity;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.HashMap;

public class WebScanningAssessment implements Parcelable {
    private String assessment_name;
    private int nikto_id;
    private int output_nikto_id;
    private String vulnerability;
    private int user_id;
    private int target_id;
    private String ip_address;
    private String hostname;
    private String port;
    private String banner;
    private String items_tested;
    private String items_found;
    private String Date;

    public static final Creator<WebScanningAssessment> CREATOR = new Creator<WebScanningAssessment>() {
        @Override
        public WebScanningAssessment createFromParcel(Parcel in) {
            return new WebScanningAssessment(in);
        }

        @Override
        public WebScanningAssessment[] newArray(int size) {
            return new WebScanningAssessment[size];
        }
    };

    public WebScanningAssessment() {
    }

    public WebScanningAssessment(Parcel in) {
        assessment_name = in.readString();
        nikto_id = in.readInt();
        output_nikto_id = in.readInt();
        vulnerability = in.readString();
        user_id = in.readInt();
        target_id = in.readInt();
        ip_address = in.readString();
        hostname = in.readString();
        port = in.readString();
        banner = in.readString();
        items_tested = in.readString();
        items_found = in.readString();
        Date = in.readString();
    }

    public String getAssessment_name() {
        return assessment_name;
    }

    public void setAssessment_name(String assessment_name) {
        this.assessment_name = assessment_name;
    }

    public String getBanner() {
        return banner;
    }

    public String getDate() {
        return Date;
    }

    public String getHostname() {
        return hostname;
    }

    public String getIp_address() {
        return ip_address;
    }

    public void setIp_address(String ip_address) {
        this.ip_address = ip_address;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public void setDate(String date) {
        Date = date;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getTarget_id() {
        return target_id;
    }

    public void setTarget_id(int target_id) {
        this.target_id = target_id;
    }

    public String getItems_found() {
        return items_found;
    }

    public void setItems_found(String items_found) {
        this.items_found = items_found;
    }

    public String getItems_tested() {
        return items_tested;
    }

    public void setItems_tested(String items_tested) {
        this.items_tested = items_tested;
    }

    public int getNikto_id() {
        return nikto_id;
    }

    public void setNikto_id(int nikto_id) {
        this.nikto_id = nikto_id;
    }

    public int getOutput_nikto_id() {
        return output_nikto_id;
    }

    public void setOutput_nikto_id(int output_nikto_id) {
        this.output_nikto_id = output_nikto_id;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public void setBanner(String banner) {
        this.banner = banner;
    }

    public String getVulnerability() {
        return vulnerability;
    }

    public void setVulnerability(String vulnerability) {
        this.vulnerability = vulnerability;
    }

    public static void convertToHashMapList(ArrayList<WebScanningAssessment> assessList,
                                            ArrayList<HashMap<String, String>> assessMapList) {
        for (WebScanningAssessment webScanningAssessment : assessList) {
            HashMap<String, String> temp = new HashMap<>();
            temp.put("output_nikto_id", String.valueOf(webScanningAssessment.output_nikto_id));
            temp.put("nikto_id", String.valueOf(webScanningAssessment.nikto_id));
            temp.put("vulnerability", webScanningAssessment.getVulnerability());
            assessMapList.add(temp);
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(assessment_name);
        dest.writeInt(nikto_id);
        dest.writeInt(output_nikto_id);
        dest.writeString(vulnerability);
        dest.writeInt(user_id);
        dest.writeInt(target_id);
        dest.writeString(ip_address);
        dest.writeString(hostname);
        dest.writeString(port);
        dest.writeString(banner);
        dest.writeString(items_tested);
        dest.writeString(items_found);
        dest.writeString(Date);
    }
}
