package com.ddrzl.vatproject.dashboard.fragment;

import android.app.AlertDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.ddrzl.vatproject.R;
import com.ddrzl.vatproject.dashboard.adapter.AssessmentListAdapter;
import com.ddrzl.vatproject.dashboard.listener.OnClickCallback;
import com.ddrzl.vatproject.dashboard.service.AssessmentService;
import com.ddrzl.vatproject.dashboard.viewmodel.AssessmentListViewModel;
import com.ddrzl.vatproject.databinding.FragmentMainBinding;
import com.ddrzl.vatproject.login.entity.User;
import com.ddrzl.vatproject.utils.HTTPUtil;
import com.ddrzl.vatproject.utils.UserManager;
import com.github.jlmd.animatedcircleloadingview.AnimatedCircleLoadingView;

import java.util.Objects;

import static android.view.View.GONE;
import static com.ddrzl.vatproject.utils.ConstantUtil.ASSESSMENT_TOOLS;
import static com.ddrzl.vatproject.utils.ConstantUtil.NETWORK_SCANNING;
import static com.ddrzl.vatproject.utils.ConstantUtil.WEB_SCANNING;

public class MainFragment extends Fragment {
    private AnimatedCircleLoadingView animatedCircleLoadingView;
    private FragmentMainBinding binding;
    private AssessmentListAdapter adapter;
    private AssessmentListViewModel viewModel;
    private User user;

    public MainFragment() {
        // Required empty public constructor
    }

    public static MainFragment newInstance() {
        MainFragment fragment = new MainFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        user = UserManager.getInstance(getContext()).getUser();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_main, container, false);
        binding.setCallback(new OnClickCallback());

        binding.lstAssessment.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.lstAssessment.setItemAnimator(new DefaultItemAnimator());
        adapter = new AssessmentListAdapter();
        binding.lstAssessment.setAdapter(adapter);

        binding.btnInsertAssessment.setOnClickListener(v -> {
            showPopUpInsertNewAssessment();
        });

//        animatedCircleLoadingView = binding.circleLoadingView;

        return binding.getRoot();
    }

    private void startLoading() {
        animatedCircleLoadingView.startDeterminate();
        animatedCircleLoadingView.setPercent(100);
    }

    private void startPercentMockThread() {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1500);
                    for (int i = 0; i <= 100; i++) {
                        Thread.sleep(65);
                        changePercent(i);
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        new Thread(runnable).start();
    }

    private void changePercent(final int percent) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                animatedCircleLoadingView.setPercent(percent);
            }
        });
    }

    public void resetLoading() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                animatedCircleLoadingView.resetLoading();
            }
        });
    }

    private void showProgress(final boolean show) {
        if (show) {
            binding.assessmentListLayout.setVisibility(GONE);
            startLoading();
            startPercentMockThread();
        } else {
            binding.assessmentListLayout.setVisibility(View.VISIBLE);
            animatedCircleLoadingView.setVisibility(GONE);
        }

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        AssessmentListViewModel.Factory factory = new AssessmentListViewModel.Factory(
                Objects.requireNonNull(getActivity()).getApplication(), user.getUser_id());
        viewModel = ViewModelProviders.of(this, factory).get(AssessmentListViewModel.class);
        observeViewModel(viewModel);
    }

    public void observeViewModel(AssessmentListViewModel viewModel) {
        viewModel.getTargetLiveData().observe(this, targets -> {
            if (targets != null) {
                adapter.setTargetList(targets);
                adapter.notifyDataSetChanged();
            }
        });
    }

    private void showPopUpInsertNewAssessment() {
        LayoutInflater layoutInflater = getLayoutInflater();
        View infoDialogLayout = layoutInflater.inflate(R.layout.dialog_insert_assessment, null);
        TextView assessment_name = infoDialogLayout.findViewById(R.id.et_insert_assessment_name);
        TextView assessment_target = infoDialogLayout.findViewById(R.id.et_insert_new_target);
        Spinner spinnerTool = infoDialogLayout.findViewById(R.id.spinner_tool);

        ArrayAdapter<CharSequence> adapterTool = new ArrayAdapter<>(Objects.requireNonNull(getActivity()),
                android.R.layout.simple_spinner_item, ASSESSMENT_TOOLS);
        adapterTool.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerTool.setAdapter(adapterTool);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.AlertDialogTheme);
        builder.setView(infoDialogLayout)
                .setPositiveButton("Insert", ((dialog, which) -> {
                    String name = assessment_name.getText().toString();
                    String target = assessment_target.getText().toString();

                    boolean cancel = false;
                    View focusView = null;

                    if ((!HTTPUtil.isNetworkAvailable(Objects.requireNonNull(getActivity())))) {
                        Toast.makeText(getActivity(), R.string.no_internet_connection, Toast.LENGTH_SHORT).show();
                        cancel = true;
                    }

                    if (assessment_name.getText().toString().isEmpty()) {
                        assessment_name.setError(getString(R.string.error_field_required));
                        focusView = assessment_name;
                        cancel = true;
                    }

                    if (assessment_target.getText().toString().isEmpty()) {
                        assessment_target.setError(getString(R.string.error_field_required));
                        focusView = assessment_target;
                        cancel = true;
                    }

                    if (spinnerTool.getSelectedItemPosition() == 0) {
                        TextView errorText = (TextView) spinnerTool.getSelectedView();
                        errorText.setError("");
                        errorText.setTextColor(Color.RED);
                        focusView = spinnerTool;
                        cancel = true;
                    }

                    if (cancel) {
                        focusView.requestFocus();
                    } else {
                        assessment_name.clearFocus();
                        assessment_target.clearFocus();
                        InputMethodManager in = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        assert in != null;
                        in.hideSoftInputFromWindow(assessment_name.getWindowToken(), 0);
                        in.hideSoftInputFromWindow(assessment_target.getWindowToken(), 0);

                        if (spinnerTool.getSelectedItemId() == NETWORK_SCANNING) {
                            new AssessmentService().createNewAssessment(getContext(), name, target, 1);
                        }
                        if (spinnerTool.getSelectedItemId() == WEB_SCANNING) {
                            new AssessmentService().createNewAssessment(getContext(), name, target, 2);
                        }
                    }
                }
                ));
        builder.setNeutralButton("Cancel", null)
                .show();
    }
}
