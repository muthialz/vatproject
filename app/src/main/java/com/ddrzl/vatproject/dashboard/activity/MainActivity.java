package com.ddrzl.vatproject.dashboard.activity;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;

import com.ddrzl.vatproject.R;
import com.ddrzl.vatproject.dashboard.fragment.DashboardFragment;
import com.ddrzl.vatproject.dashboard.fragment.MainFragment;
import com.ddrzl.vatproject.dashboard.fragment.ProfileSettingsFragment;
import com.ddrzl.vatproject.utils.FragmentUtil;

public class MainActivity extends AppCompatActivity {
    private TextView mTextMessage;
    private Fragment selectedFragment = null;
    private ActionBar toolbar;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    selectedFragment = MainFragment.newInstance();
                    FragmentUtil.loadFragment(MainActivity.this, selectedFragment, true);
                    return true;
                case R.id.navigation_dashboard:
                    selectedFragment = DashboardFragment.newInstance();
                    FragmentUtil.loadFragment(MainActivity.this, selectedFragment, true);
                    return true;
                case R.id.navigation_profile_settings:
                    selectedFragment = ProfileSettingsFragment.newInstance();
                    FragmentUtil.loadFragment(MainActivity.this, selectedFragment, true);
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = getSupportActionBar();
        if (toolbar != null) {
            toolbar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.black)));
        }

        selectedFragment = MainFragment.newInstance();
        FragmentUtil.loadFragment(MainActivity.this, selectedFragment, false);

        mTextMessage = findViewById(R.id.message);
        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }

    @Override
    public void onBackPressed() {
        FragmentManager fm = getSupportFragmentManager();
        if (fm.getBackStackEntryCount() > 0) {
            fm.popBackStack();
            Fragment fragment = getSupportFragmentManager().findFragmentByTag("TARGET_TAG");
            if (fragment instanceof MainFragment) {
                finish();
            }
        } else {
            super.onBackPressed();
        }
    }
}
