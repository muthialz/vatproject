package com.ddrzl.vatproject.dashboard.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.ddrzl.vatproject.R;
import com.ddrzl.vatproject.dashboard.entity.WebScanningAssessment;
import com.ddrzl.vatproject.dashboard.listener.OnClickCallback;
import com.ddrzl.vatproject.databinding.ItemNiktoHistoryBinding;

import java.util.List;
import java.util.Objects;

public class NiktoHistoryAdapter extends RecyclerView.Adapter<NiktoHistoryAdapter.ViewHolder> {
    private ItemNiktoHistoryBinding binding;
    private List<? extends WebScanningAssessment> web_scanning_histories;

    public void setWeb_scanning_histories(List<? extends WebScanningAssessment> web_scanning_histories) {
        this.web_scanning_histories = web_scanning_histories;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public NiktoHistoryAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = (LayoutInflater) viewGroup.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        binding = DataBindingUtil.inflate(Objects.requireNonNull(inflater), R.layout.item_nikto_history, viewGroup, false);
        binding.setCallback(new OnClickCallback());
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull NiktoHistoryAdapter.ViewHolder viewHolder, int i) {
        viewHolder.binding.setHistory(web_scanning_histories.get(i));
        viewHolder.binding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return (web_scanning_histories == null ? 0 : web_scanning_histories.size());
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        final ItemNiktoHistoryBinding binding;

        public ViewHolder(ItemNiktoHistoryBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
