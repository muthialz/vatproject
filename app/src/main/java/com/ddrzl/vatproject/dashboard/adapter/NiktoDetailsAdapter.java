package com.ddrzl.vatproject.dashboard.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.ddrzl.vatproject.R;
import com.ddrzl.vatproject.dashboard.entity.WebScanningAssessment;
import com.ddrzl.vatproject.databinding.ItemNiktoScanResultBinding;

import java.util.List;
import java.util.Objects;

public class NiktoDetailsAdapter extends RecyclerView.Adapter<NiktoDetailsAdapter.ViewHolder> {
    private List<? extends WebScanningAssessment> webScanningAssessments;
    private ItemNiktoScanResultBinding binding;

    public void setWebScanningAssessments(List<? extends WebScanningAssessment> webScanningAssessments) {
        this.webScanningAssessments = webScanningAssessments;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public NiktoDetailsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = (LayoutInflater) viewGroup.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        binding = DataBindingUtil.inflate(Objects.requireNonNull(inflater), R.layout.item_nikto_scan_result, viewGroup, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull NiktoDetailsAdapter.ViewHolder viewHolder, int i) {
        viewHolder.binding.setResult(webScanningAssessments.get(i));
        viewHolder.binding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return (webScanningAssessments == null ? 0 : webScanningAssessments.size());
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        final ItemNiktoScanResultBinding binding;

        public ViewHolder(@NonNull ItemNiktoScanResultBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
