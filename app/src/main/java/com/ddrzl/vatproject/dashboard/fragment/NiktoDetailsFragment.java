package com.ddrzl.vatproject.dashboard.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.pdf.PdfDocument;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.ddrzl.vatproject.R;
import com.ddrzl.vatproject.dashboard.adapter.NiktoDetailsAdapter;
import com.ddrzl.vatproject.dashboard.entity.WebScanningAssessment;
import com.ddrzl.vatproject.dashboard.viewmodel.NiktoDetailsViewModel;
import com.ddrzl.vatproject.databinding.FragmentNiktoDetailsBinding;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import lecho.lib.hellocharts.model.PieChartData;
import lecho.lib.hellocharts.model.SliceValue;

import static java.lang.Math.round;

public class NiktoDetailsFragment extends Fragment {
    public int target_id, output_nikto_id;
    private FragmentNiktoDetailsBinding binding;
    private NiktoDetailsViewModel viewModel;
    private NiktoDetailsAdapter adapter;
    private Bitmap bitmap;
    private LinearLayout layoutResult;
    private PieChartData pieChartData;
    private List<WebScanningAssessment> assessmentList;

    public static NiktoDetailsFragment newInstance(int target_id, int output_nikto_id, List<WebScanningAssessment> assessmentList) {
        NiktoDetailsFragment fragment = new NiktoDetailsFragment();
        Bundle args = new Bundle();
        args.putInt("target_id", target_id);
        args.putInt("output_nikto_id", output_nikto_id);
        args.putParcelableArrayList("assessmentList", (ArrayList<? extends Parcelable>) assessmentList);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            target_id = getArguments().getInt("target_id");
            output_nikto_id = getArguments().getInt("output_nikto_id");
            assessmentList = getArguments().getParcelableArrayList("assessmentList");
        }
    }

    public static Bitmap loadBitmapFromView(View v, int width, int height) {
        Bitmap b = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(b);
        v.draw(c);

        return b;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_nikto_details, container, false);
        binding.lstResult.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.lstResult.setItemAnimator(new DefaultItemAnimator());
        adapter = new NiktoDetailsAdapter();
        binding.lstResult.setAdapter(adapter);

        layoutResult = binding.layoutResult;

        if (ContextCompat.checkSelfPermission(Objects.requireNonNull(getContext()), Manifest.permission.READ_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_DENIED) {
            ActivityCompat.requestPermissions(Objects.requireNonNull(getActivity()), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 123);
        }

        if (ContextCompat.checkSelfPermission(Objects.requireNonNull(getContext()), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_DENIED) {
            ActivityCompat.requestPermissions(Objects.requireNonNull(getActivity()), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 101);
        }

        binding.btnDownloadReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("size", " " + binding.lstResult.getWidth() + "  " + binding.lstResult.getWidth());
                bitmap = loadBitmapFromView(layoutResult, layoutResult.getWidth(), layoutResult.getHeight());
                generateViewToPDF();
            }
        });

        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        NiktoDetailsViewModel.Factory factory = new NiktoDetailsViewModel.Factory(
                Objects.requireNonNull(getActivity()).getApplication(), target_id, output_nikto_id);
        viewModel = ViewModelProviders.of(this, factory).get(NiktoDetailsViewModel.class);
        observeViewModel(viewModel);
    }

    public void observeViewModel(NiktoDetailsViewModel viewModel) {
        viewModel.getAssessmentList().observe(this, details -> {
            if (details != null) {
                adapter.setWebScanningAssessments(details);
                adapter.notifyDataSetChanged();
            }

            binding.tvTargetHostname.setText("Target Hostname: " + details.get(0).getHostname());
            binding.tvTargetIp.setText("Target IP Address: " + details.get(0).getIp_address());
            binding.tvTargetDate.setText("Scanned On: " + details.get(0).getDate());
            binding.tvItemsScanned.setText("Items Scanned: " + details.get(0).getItems_tested());
            binding.tvItemsFound.setText("Items Found: " + details.get(0).getItems_found());
            binding.tvTargetBanner.setText("Banner: " + details.get(0).getBanner());
            binding.tvTargetPort.setText("Port Opened: " + details.get(0).getPort());
            binding.tvAssessmentName.setText(details.get(0).getAssessment_name());

            int scanned = Integer.parseInt(details.get(0).getItems_tested());
            int found = Integer.parseInt(details.get(0).getItems_found());
            String hostname = details.get(0).getHostname();
//                int percent = (found / scanned) * 100;
            setPieChartData(scanned, found, hostname);
        });
    }

    private void setPieChartData(int scanned, int found, String hostname) {
        float found_percent = (found / scanned) * 100;
        float not_found_percent = 100 - found_percent;
        float founds = round(found_percent);
        float not_found = round(not_found_percent);

        List pieData = new ArrayList<>();
        pieData.add(new SliceValue(not_found_percent, Color.parseColor("#240d39")).setLabel("Items Scanned: " + scanned));
        pieData.add(new SliceValue(2, Color.parseColor("#693f7b")).setLabel("Vulnerabilities: " + found));

        String pie_target_name = hostname;
        pieChartData = new PieChartData(pieData);
        pieChartData.setHasLabels(true).setValueLabelTextSize(10);
        pieChartData.setHasCenterCircle(true).setCenterText1(pie_target_name).setCenterText1FontSize(16).
                setCenterText1Color(Color.parseColor("#04151F"));

        binding.chart.setPieChartData(pieChartData);
    }

    public void generateViewToPDF() {
        WindowManager wm = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
        //  Display display = wm.getDefaultDisplay();
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        float height = displaymetrics.heightPixels;
        float width = displaymetrics.widthPixels;

        int convertHeight = (int) height, convertWidth = (int) width;

//        Resources mResources = getResources();
//        Bitmap bitmap = BitmapFactory.decodeResource(mResources, R.drawable.screenshot);

        PdfDocument document = new PdfDocument();
        PdfDocument.PageInfo pageInfo = new PdfDocument.PageInfo.Builder(convertWidth, convertHeight, 2).create();
        PdfDocument.Page page = document.startPage(pageInfo);

        Canvas canvas = page.getCanvas();

        Paint paint = new Paint();
        canvas.drawPaint(paint);

        bitmap = Bitmap.createScaledBitmap(bitmap, convertWidth, convertHeight, true);

        paint.setColor(Color.WHITE);
        canvas.drawBitmap(bitmap, 0, 0, null);
        document.finishPage(page);

        // write the document content
        File filePath;
        String fileName = "/target_id_" + target_id + ".pdf";
        filePath = new File(getActivity().getExternalFilesDir("report"), fileName);
        try {
            document.writeTo(new FileOutputStream(filePath));

        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(getContext(), "Something wrong: " + e.toString(), Toast.LENGTH_LONG).show();
        }

        // close the document
        document.close();

        AlertDialog.Builder builder = new AlertDialog.Builder(Objects.requireNonNull(getContext()), R.style.AlertDialogTheme);
        builder.setMessage("Do you want to open the file?");
        builder.setPositiveButton("Open", ((dialog, which) -> {
            openGeneratedPDF();
        }));
        builder.setNegativeButton(android.R.string.cancel, (dialog, id) -> dialog.cancel());
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void openGeneratedPDF() {
        String fileName = "/target_id_" + target_id + ".pdf";
        File file = new File(getActivity().getExternalFilesDir("report"), fileName);
        if (file.exists()) {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            Uri uri = Uri.fromFile(file);
            intent.setDataAndType(uri, "application/pdf");
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            try {
                startActivity(intent);
            } catch (ActivityNotFoundException e) {
                Toast.makeText(getContext(), "No Application available to view pdf", Toast.LENGTH_LONG).show();
            }
        }
    }

    public boolean generatePDF() {
        Document document = new Document();
        try {
            //Create file path for Pdf
            String path = "/sdcard/";
            File dirFile = new File(path);
            if (!dirFile.exists()) {
                dirFile.createNewFile();
            }

            Log.d("PDFCreator", "PDF Path: " + path);

            String fileName = "/target_id_" + target_id + ".pdf";
            File file = new File(getActivity().getExternalFilesDir("report"), fileName);

            // create an instance of itext document
            PdfWriter.getInstance(document,
                    new FileOutputStream(file.getAbsoluteFile()));
            document.open();

            /* Create Paragraph and Set Font */
            Paragraph title_paragraph = new Paragraph(String.valueOf(binding.tvAssessmentName.getText()));
            Font paraFont = new Font(Font.FontFamily.TIMES_ROMAN);
            paraFont.setSize(20);
            paraFont.isBold();
            title_paragraph.setAlignment(Paragraph.ALIGN_CENTER);
            title_paragraph.setFont(paraFont);
            document.add(title_paragraph);

            Paragraph scanned_date_paragraph = new Paragraph(String.valueOf(binding.tvTargetDate.getText()));
            Font paraFont3 = new Font(Font.FontFamily.TIMES_ROMAN);
            paraFont3.setSize(12);
            scanned_date_paragraph.setAlignment(Paragraph.ALIGN_LEFT);
            scanned_date_paragraph.setFont(paraFont);
            document.add(scanned_date_paragraph);

            Paragraph target_paragraph = new Paragraph(String.valueOf(binding.tvTargetHostname.getText()));
            Font paraFont2 = new Font(Font.FontFamily.TIMES_ROMAN);
            paraFont2.setSize(12);
            target_paragraph.setAlignment(Paragraph.ALIGN_LEFT);
            target_paragraph.setFont(paraFont);
            document.add(target_paragraph);

            Paragraph ip_address_paragraph = new Paragraph(String.valueOf(binding.tvTargetIp.getText()));
            Font ip_address_font = new Font(Font.FontFamily.TIMES_ROMAN);
            ip_address_font.setSize(12);
            ip_address_paragraph.setAlignment(Paragraph.ALIGN_LEFT);
            ip_address_paragraph.setFont(paraFont);
            document.add(ip_address_paragraph);

            Paragraph banner_paragraph = new Paragraph(String.valueOf(binding.tvTargetBanner.getText()));
            Font banner_font = new Font(Font.FontFamily.TIMES_ROMAN);
            banner_font.setSize(12);
            banner_paragraph.setAlignment(Paragraph.ALIGN_LEFT);
            banner_paragraph.setFont(paraFont);
            document.add(banner_paragraph);

            Paragraph port_opened_paragraph = new Paragraph(String.valueOf(binding.tvTargetPort.getText()));
            Font port_opened_font = new Font(Font.FontFamily.TIMES_ROMAN);
            port_opened_font.setSize(12);
            port_opened_paragraph.setAlignment(Paragraph.ALIGN_LEFT);
            port_opened_paragraph.setFont(paraFont);
            document.add(port_opened_paragraph);

            Paragraph items_scanned_paragraph = new Paragraph(String.valueOf(binding.tvItemsScanned.getText()));
            Font items_scanned_font = new Font(Font.FontFamily.TIMES_ROMAN);
            items_scanned_font.setSize(12);
            items_scanned_paragraph.setAlignment(Paragraph.ALIGN_LEFT);
            items_scanned_paragraph.setFont(paraFont);
            document.add(items_scanned_paragraph);

            Paragraph items_found_paragraph = new Paragraph(String.valueOf(binding.tvItemsFound.getText()));
            Font items_found_font = new Font(Font.FontFamily.TIMES_ROMAN);
            items_found_font.setSize(12);
            items_found_paragraph.setAlignment(Paragraph.ALIGN_LEFT);
            items_found_paragraph.setFont(paraFont);
            document.add(items_found_paragraph);

            Paragraph server_details_paragraph = new Paragraph("Vulnerabilities:");
            Font server_details_font = new Font(Font.FontFamily.TIMES_ROMAN);
            server_details_font.setSize(16);
            server_details_paragraph.setAlignment(Paragraph.ALIGN_LEFT);
            server_details_paragraph.setFont(paraFont);
            document.add(server_details_paragraph);

            @SuppressLint("SimpleDateFormat")
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy hh-mm-ss");
            String format = simpleDateFormat.format(new Date());
            Paragraph generated_on_paragraph = new Paragraph("Generated on: " + format);
            Font paraFont4 = new Font(Font.FontFamily.TIMES_ROMAN);
            paraFont4.setSize(10);
            generated_on_paragraph.setAlignment(Paragraph.ALIGN_LEFT);
            generated_on_paragraph.setFont(paraFont);
            document.add(generated_on_paragraph);

            Paragraph footer = new Paragraph("Report generated by: VATool");
            Font paraFont5 = new Font(Font.FontFamily.TIMES_ROMAN);
            paraFont5.setSize(10);
            footer.setAlignment(Paragraph.ALIGN_BOTTOM);
            footer.setFont(paraFont);
            document.add(footer);

            AlertDialog.Builder builder = new AlertDialog.Builder(Objects.requireNonNull(getContext()), R.style.AlertDialogTheme);
            builder.setMessage("Do you want to open the file?");
            builder.setPositiveButton("Open", ((dialog, which) -> {
                openPDF();
            }));
            builder.setNegativeButton(android.R.string.cancel, (dialog, id) -> dialog.cancel());
            AlertDialog alertDialog = builder.create();
            alertDialog.show();

            // close document
            document.close();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        } catch (DocumentException e) {
            e.printStackTrace();
            return false;
        }
    }

    public void openPDF() {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        String path = "/sdcard/";
        String fileName = "/target_id_" + target_id + ".pdf";
        File file = new File(getActivity().getExternalFilesDir("report"), fileName);

        intent.setDataAndType(Uri.fromFile(file), "application/pdf");
        try {
            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(getContext(), "No Application available to view pdf", Toast.LENGTH_LONG).show();
        }
    }
}
