package com.ddrzl.vatproject.dashboard.service;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.ddrzl.vatproject.R;
import com.ddrzl.vatproject.dashboard.entity.NetworkScanningAssessment;
import com.ddrzl.vatproject.utils.JSONUtil;
import com.ddrzl.vatproject.volley.VolleyCallback;
import com.ddrzl.vatproject.volley.VolleyRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import static com.ddrzl.vatproject.utils.UrlUtil.BASE_URL;
import static com.ddrzl.vatproject.utils.UrlUtil.URL_GET_NMAP_DETAILS;

public class NmapDetailsListService {
    private static NmapDetailsListService nmapDetailsListService;
    private MutableLiveData<List<NetworkScanningAssessment>> networkListLiveData;
    private WeakReference<Context> mContextRef;
    private VolleyRequest request = new VolleyRequest();
    private JSONUtil jsonUtil = new JSONUtil();

    private NmapDetailsListService(Context context) {
        mContextRef = new WeakReference<>(context);
        networkListLiveData = new MutableLiveData<>();
    }

    public synchronized static NmapDetailsListService getService(Context context) {
        if (nmapDetailsListService == null) {
            nmapDetailsListService = new NmapDetailsListService(context);
        }
        return nmapDetailsListService;
    }

    public LiveData<List<NetworkScanningAssessment>> getNmapDetails(int target_id, int output_nmap_id) {
        String url = BASE_URL + URL_GET_NMAP_DETAILS;
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("target_id", target_id);
            jsonObject.put("output_nmap_id", output_nmap_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        request.postJSONObjectRequest(mContextRef.get(), url, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccessResponse(String result) {
                List<NetworkScanningAssessment> historyList = new ArrayList<>();
                try {
                    JSONObject jsonResult = new JSONObject(result);
                    if (jsonResult.getBoolean("status")) {
                        JSONArray jsonArray = jsonResult.getJSONArray("Report");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            NetworkScanningAssessment details = new NetworkScanningAssessment();
                            jsonUtil.toObj(details, jsonArray.getJSONObject(i));
                            historyList.add(details);
                        }
                        networkListLiveData.setValue(historyList);
                    } else {
                        Toast.makeText(mContextRef.get(), R.string.failed_to_get_assessment_details, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                networkListLiveData.setValue(historyList);
            }

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(mContextRef.get(), R.string.failed_to_get_assessment_details, Toast.LENGTH_LONG).show();
                Log.e("ERROR", "Error occurred ", error);
            }
        });

        return networkListLiveData;
    }
}
