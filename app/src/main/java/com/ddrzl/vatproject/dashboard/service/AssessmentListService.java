package com.ddrzl.vatproject.dashboard.service;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.ddrzl.vatproject.R;
import com.ddrzl.vatproject.dashboard.entity.Target;
import com.ddrzl.vatproject.utils.JSONUtil;
import com.ddrzl.vatproject.volley.VolleyCallback;
import com.ddrzl.vatproject.volley.VolleyRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import static com.ddrzl.vatproject.utils.UrlUtil.BASE_URL;
import static com.ddrzl.vatproject.utils.UrlUtil.URL_GET_TARGET_LIST;

public class AssessmentListService {
    private static AssessmentListService assessmentListService;
    private MutableLiveData<List<Target>> targetListLiveData;
    private WeakReference<Context> mContextRef;
    private VolleyRequest request = new VolleyRequest();
    private JSONUtil jsonUtil = new JSONUtil();

    private AssessmentListService(Context context) {
        mContextRef = new WeakReference<>(context);
        targetListLiveData = new MutableLiveData<>();
    }

    public synchronized static AssessmentListService getService(Context context) {
        if (assessmentListService == null) {
            assessmentListService = new AssessmentListService(context);
        }
        return assessmentListService;
    }

    public LiveData<List<Target>> getTargetList(int user_id) {
        String url = BASE_URL + URL_GET_TARGET_LIST;
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("user_id", user_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        request.postJSONObjectRequest(mContextRef.get(), url, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccessResponse(String result) {
                List<Target> targetList = new ArrayList<>();
                try {
                    JSONObject jsonResult = new JSONObject(result);
                    if (jsonResult.getBoolean("status")) {
                        JSONArray jsonArray = jsonResult.getJSONArray("Assessments Target");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            Target target = new Target();
                            jsonUtil.toObj(target, jsonArray.getJSONObject(i));
                            targetList.add(target);
                        }
                        targetListLiveData.setValue(targetList);
                    } else {
                        Toast.makeText(mContextRef.get(), R.string.failed_get_assignment_list, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                targetListLiveData.setValue(targetList);
            }

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(mContextRef.get(), R.string.failed_get_assignment_list, Toast.LENGTH_LONG).show();
                Log.e("ERROR", "Error occurred ", error);
            }
        });

        return targetListLiveData;
    }
}
