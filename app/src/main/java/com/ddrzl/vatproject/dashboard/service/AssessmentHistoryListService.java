package com.ddrzl.vatproject.dashboard.service;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.ddrzl.vatproject.R;
import com.ddrzl.vatproject.dashboard.entity.NetworkScanningAssessment;
import com.ddrzl.vatproject.dashboard.entity.WebScanningAssessment;
import com.ddrzl.vatproject.utils.JSONUtil;
import com.ddrzl.vatproject.volley.VolleyCallback;
import com.ddrzl.vatproject.volley.VolleyRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import static com.ddrzl.vatproject.utils.UrlUtil.BASE_URL;
import static com.ddrzl.vatproject.utils.UrlUtil.URL_GET_ASSESSMENT_HISTORY;

public class AssessmentHistoryListService {
    private static AssessmentHistoryListService historyListService;
    private MutableLiveData<List<NetworkScanningAssessment>> networkListLiveData;
    private MutableLiveData<List<WebScanningAssessment>> webListLiveData;
    private WeakReference<Context> mContextRef;
    private VolleyRequest request = new VolleyRequest();
    private JSONUtil jsonUtil = new JSONUtil();

    private AssessmentHistoryListService(Context context) {
        mContextRef = new WeakReference<>(context);
        networkListLiveData = new MutableLiveData<>();
        webListLiveData = new MutableLiveData<>();
    }

    public synchronized static AssessmentHistoryListService getService(Context context) {
        if (historyListService == null) {
            historyListService = new AssessmentHistoryListService(context);
        }
        return historyListService;
    }

    public LiveData<List<NetworkScanningAssessment>> getHistoryList(int user_id) {
        String url = BASE_URL + URL_GET_ASSESSMENT_HISTORY;
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("user_id", user_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        request.postJSONObjectRequest(mContextRef.get(), url, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccessResponse(String result) {
                List<NetworkScanningAssessment> historyList = new ArrayList<>();
                try {
                    JSONObject jsonResult = new JSONObject(result);
                    if (jsonResult.getBoolean("status")) {
                        JSONArray jsonArray = jsonResult.getJSONArray("Network Vulnerability Assessments");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            NetworkScanningAssessment history = new NetworkScanningAssessment();
                            jsonUtil.toObj(history, jsonArray.getJSONObject(i));
                            historyList.add(history);
                        }
                        networkListLiveData.setValue(historyList);
                    } else {
                        Toast.makeText(mContextRef.get(), R.string.failed_to_get_assessment_history, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                networkListLiveData.setValue(historyList);
            }

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(mContextRef.get(), R.string.failed_to_get_assessment_history, Toast.LENGTH_LONG).show();
                Log.e("ERROR", "Error occurred ", error);
            }
        });

        return networkListLiveData;
    }

    public LiveData<List<WebScanningAssessment>> getWebHistoryList(int user_id) {
        String url = BASE_URL + URL_GET_ASSESSMENT_HISTORY;
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("user_id", user_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        request.postJSONObjectRequest(mContextRef.get(), url, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccessResponse(String result) {
                List<WebScanningAssessment> historyList2 = new ArrayList<>();
                try {
                    JSONObject jsonResult = new JSONObject(result);
                    if (jsonResult.getBoolean("status")) {
                        JSONArray jsonArray2 = jsonResult.getJSONArray("Web Server Vulnerability Assessments");
                        for (int i = 0; i < jsonArray2.length(); i++) {
                            WebScanningAssessment history = new WebScanningAssessment();
                            jsonUtil.toObj(history, jsonArray2.getJSONObject(i));
                            historyList2.add(history);
                        }
                        webListLiveData.setValue(historyList2);
                    } else {
                        Toast.makeText(mContextRef.get(), R.string.failed_to_get_assessment_history, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                webListLiveData.setValue(historyList2);
            }

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(mContextRef.get(), R.string.failed_to_get_assessment_history, Toast.LENGTH_LONG).show();
                Log.e("ERROR", "Error occurred ", error);
            }
        });

        return webListLiveData;
    }

}
