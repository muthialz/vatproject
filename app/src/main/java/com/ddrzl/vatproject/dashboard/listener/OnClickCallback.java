package com.ddrzl.vatproject.dashboard.listener;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.ddrzl.vatproject.R;
import com.ddrzl.vatproject.dashboard.service.AssessmentService;
import com.ddrzl.vatproject.login.LoginActivity;
import com.ddrzl.vatproject.utils.HTTPUtil;
import com.ddrzl.vatproject.utils.UserManager;

import static java.util.Objects.requireNonNull;

public class OnClickCallback {
    public boolean is_update_tool_1;

    private static AppCompatActivity scanForActivity(Context cont) {
        if (cont == null)
            return null;
        else if (cont instanceof AppCompatActivity)
            return (AppCompatActivity) cont;
        else if (cont instanceof ContextThemeWrapper)
            return scanForActivity(((ContextThemeWrapper) cont).getBaseContext());

        return null;
    }

    public void nmapAssessmentDetails(View view, int target_id, int output_nmap_id) {
        AssessmentService service = new AssessmentService();
        service.getNmapDetails(view.getContext(), target_id, output_nmap_id);
    }

    public void niktoAssessmentDetails(View view, int target_id, int output_nikto_id) {
        AssessmentService service = new AssessmentService();
        service.getNiktoDetails(view.getContext(), target_id, output_nikto_id);
    }

    public void signOut(View view) {
        Context context = view.getContext();

        AlertDialog.Builder builder = new AlertDialog.Builder(requireNonNull(context), R.style.AlertDialogTheme);
        builder.setMessage(R.string.ask_log_out);
        builder.setPositiveButton(android.R.string.ok, ((dialog, which) -> {
            UserManager.getInstance(context).logoutUser();
            Intent intent = new Intent(context, LoginActivity.class);
            //prevent backstack after logout
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        }));
        builder.setNegativeButton(android.R.string.cancel, (dialog, id) -> dialog.cancel());
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    public void changePassword(View view) {
        Context context = view.getContext();
        Activity act = scanForActivity(context);
        LayoutInflater layoutInflater = act.getLayoutInflater();
        View infoDialogLayout = layoutInflater.inflate(R.layout.dialog_forgot_password, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(requireNonNull(context), R.style.AlertDialogTheme);
        builder.setView(infoDialogLayout)
                .setTitle("Forgot Password")
                .setPositiveButton("Change Password", ((dialog, which) -> {
                    Toast.makeText(context, "Password changed. Please sign in with your new password", Toast.LENGTH_SHORT).show();
                }
                ));
        builder.show();
    }

    public void scanAssessment(View view, int target_id, int user_id, int tool_id) {
        if (tool_id == 1) {
            AssessmentService service = new AssessmentService();
            service.networkScanning(view.getContext(), target_id, user_id);
        } else {
            AssessmentService service = new AssessmentService();
            service.webScanning(view.getContext(), target_id, user_id);
        }
    }

    public void showPopUpManageAssessment(View view, int target_id, String old_assessment_name, String old_assessment_ip, String old_assessment_hostname) {
        Context context = view.getContext();
        Activity act = scanForActivity(context);

        LayoutInflater layoutInflater = act.getLayoutInflater();
        View manageAssessmentDialogLayout = layoutInflater.inflate(R.layout.dialog_manage_assessment, null);
        EditText assessment_name = manageAssessmentDialogLayout.findViewById(R.id.assessment_name);
        EditText assessment_target = manageAssessmentDialogLayout.findViewById(R.id.assessment_target);

        assessment_name.setText(old_assessment_name);
//        assessment_target.setText(old_assessment_hostname);
//        assessment_target.setText(old_assessment_ip);

        if (!old_assessment_ip.isEmpty()) {
            assessment_target.setText(old_assessment_ip);
            is_update_tool_1 = true;
        }
        if (!old_assessment_hostname.isEmpty()) {
            assessment_target.setText(old_assessment_hostname);
            is_update_tool_1 = false;
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.AlertDialogTheme);
        builder.setView(manageAssessmentDialogLayout)
                .setPositiveButton("Save", ((dialog, which) -> {
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //Do nothing here because we override this button later to change the close behaviour.
                            //However, we still need this because on older versions of Android unless we
                            //pass a handler the button doesn't get instantiated
                        }
                    };
                }));
        builder.setNeutralButton("Cancel", null);
        final AlertDialog dialog = builder.create();
        dialog.show();
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String new_assessment_name = assessment_name.getText().toString();
                String new_assessment_target = assessment_target.getText().toString();

                boolean cancel = false;
                View focusView = null;

                if (!HTTPUtil.isNetworkAvailable(requireNonNull(act))) {
                    Toast.makeText(context, R.string.no_internet_connection, Toast.LENGTH_SHORT).show();
                    cancel = true;
                }

                if (assessment_name.getText().toString().isEmpty()) {
                    assessment_name.setError(context.getString(R.string.error_field_required));
                    focusView = assessment_name;
                    cancel = true;
                }

                if (assessment_target.getText().toString().isEmpty()) {
                    assessment_target.setError(context.getString(R.string.error_field_required));
                    focusView = assessment_target;
                    cancel = true;
                }

                if (cancel) {
                    requireNonNull(focusView).requestFocus();
                } else {
                    dialog.dismiss();
                    assessment_name.clearFocus();
                    assessment_target.clearFocus();
                    InputMethodManager in = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                    assert in != null;
                    in.hideSoftInputFromWindow(assessment_name.getWindowToken(), 0);
                    in.hideSoftInputFromWindow(assessment_target.getWindowToken(), 0);

                    if (is_update_tool_1) {
                        AssessmentService assessmentService = new AssessmentService();
                        assessmentService.updateAssessment(context, true, target_id, new_assessment_target, new_assessment_name);
                    } else {
                        AssessmentService assessmentService = new AssessmentService();
                        assessmentService.updateAssessment(context, false, target_id, new_assessment_target, new_assessment_name);
                    }
                }
                //else dialog stays open. Make sure you have an obvious way to close the dialog especially if you set cancellable to false.
            }
        });
    }
}
