package com.ddrzl.vatproject.dashboard.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.ddrzl.vatproject.R;
import com.ddrzl.vatproject.dashboard.entity.Target;
import com.ddrzl.vatproject.dashboard.listener.OnClickCallback;
import com.ddrzl.vatproject.databinding.ItemAssessmentListBinding;

import java.util.List;
import java.util.Objects;

public class AssessmentListAdapter extends RecyclerView.Adapter<AssessmentListAdapter.ViewHolder> {
    private List<? extends Target> targetList;
    private ItemAssessmentListBinding binding;

    public void setTargetList(List<? extends Target> targetList) {
        this.targetList = targetList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public AssessmentListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = (LayoutInflater) viewGroup.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        binding = DataBindingUtil.inflate(Objects.requireNonNull(inflater), R.layout.item_assessment_list, viewGroup, false);
        binding.setCallback(new OnClickCallback());

        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull AssessmentListAdapter.ViewHolder viewHolder, int i) {
        viewHolder.binding.setTarget(targetList.get(i));
        viewHolder.binding.executePendingBindings();
    }


    @Override
    public int getItemCount() {
        return (targetList == null ? 0 : targetList.size());
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        final ItemAssessmentListBinding binding;

        public ViewHolder(ItemAssessmentListBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
