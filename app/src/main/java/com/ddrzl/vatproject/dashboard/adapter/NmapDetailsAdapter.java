package com.ddrzl.vatproject.dashboard.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.ddrzl.vatproject.R;
import com.ddrzl.vatproject.dashboard.entity.NetworkScanningAssessment;
import com.ddrzl.vatproject.databinding.ItemNmapScanResultBinding;

import java.util.List;
import java.util.Objects;

public class NmapDetailsAdapter extends RecyclerView.Adapter<NmapDetailsAdapter.ViewHolder> {
    private List<? extends NetworkScanningAssessment> networkScanningAssessments;
    private ItemNmapScanResultBinding binding;

    public void setNetworkScanningAssessments(List<? extends NetworkScanningAssessment> networkScanningAssessments) {
        this.networkScanningAssessments = networkScanningAssessments;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public NmapDetailsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = (LayoutInflater) viewGroup.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        binding = DataBindingUtil.inflate(Objects.requireNonNull(inflater), R.layout.item_nmap_scan_result, viewGroup, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull NmapDetailsAdapter.ViewHolder viewHolder, int i) {
        viewHolder.binding.setResult(networkScanningAssessments.get(i));
        viewHolder.binding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return (networkScanningAssessments == null ? 0 : networkScanningAssessments.size());
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        final ItemNmapScanResultBinding binding;

        public ViewHolder(@NonNull ItemNmapScanResultBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
