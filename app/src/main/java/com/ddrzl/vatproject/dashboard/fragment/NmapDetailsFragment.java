package com.ddrzl.vatproject.dashboard.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.ddrzl.vatproject.R;
import com.ddrzl.vatproject.dashboard.adapter.NmapDetailsAdapter;
import com.ddrzl.vatproject.dashboard.viewmodel.NmapDetailsViewModel;
import com.ddrzl.vatproject.databinding.FragmentNetworkScanningDetailsBinding;
import com.ddrzl.vatproject.login.entity.User;
import com.ddrzl.vatproject.utils.UserManager;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

import static com.ddrzl.vatproject.utils.ConstantUtil.PORT_110;
import static com.ddrzl.vatproject.utils.ConstantUtil.PORT_110_TITLE;
import static com.ddrzl.vatproject.utils.ConstantUtil.PORT_143;
import static com.ddrzl.vatproject.utils.ConstantUtil.PORT_143_TITLE;
import static com.ddrzl.vatproject.utils.ConstantUtil.PORT_21;
import static com.ddrzl.vatproject.utils.ConstantUtil.PORT_21_TITLE;
import static com.ddrzl.vatproject.utils.ConstantUtil.PORT_22;
import static com.ddrzl.vatproject.utils.ConstantUtil.PORT_22_TITLE;
import static com.ddrzl.vatproject.utils.ConstantUtil.PORT_23;
import static com.ddrzl.vatproject.utils.ConstantUtil.PORT_23_TITLE;
import static com.ddrzl.vatproject.utils.ConstantUtil.PORT_25;
import static com.ddrzl.vatproject.utils.ConstantUtil.PORT_25_TITLE;
import static com.ddrzl.vatproject.utils.ConstantUtil.PORT_3389;
import static com.ddrzl.vatproject.utils.ConstantUtil.PORT_3389_TITLE;
import static com.ddrzl.vatproject.utils.ConstantUtil.PORT_443;
import static com.ddrzl.vatproject.utils.ConstantUtil.PORT_443_TITLE;
import static com.ddrzl.vatproject.utils.ConstantUtil.PORT_445;
import static com.ddrzl.vatproject.utils.ConstantUtil.PORT_445_TITLE;
import static com.ddrzl.vatproject.utils.ConstantUtil.PORT_80;
import static com.ddrzl.vatproject.utils.ConstantUtil.PORT_80_TITLE;

public class NmapDetailsFragment extends Fragment {
    public int target_id, output_nmap_id;
    private FragmentNetworkScanningDetailsBinding binding;
    private NmapDetailsAdapter adapter;
    private NmapDetailsViewModel viewModel;
    private Bitmap bitmap;
    private LinearLayout layoutResult;
    private User user;

    public static NmapDetailsFragment newInstance(int target_id, int output_nmap_id) {
        NmapDetailsFragment fragment = new NmapDetailsFragment();
        Bundle args = new Bundle();
        args.putInt("target_id", target_id);
        args.putInt("output_nmap_id", output_nmap_id);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            target_id = getArguments().getInt("target_id");
            output_nmap_id = getArguments().getInt("output_nmap_id");
        }
        user = UserManager.getInstance(getActivity()).getUser();
    }

    public static Bitmap loadBitmapFromView(View v, int width, int height) {
        Bitmap b = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(b);
        v.draw(c);

        return b;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_network_scanning_details, container, false);
        binding.lstResult.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.lstResult.setItemAnimator(new DefaultItemAnimator());
        adapter = new NmapDetailsAdapter();
        binding.lstResult.setAdapter(adapter);

        layoutResult = binding.layoutResult;

        if (ContextCompat.checkSelfPermission(Objects.requireNonNull(getContext()), Manifest.permission.READ_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_DENIED) {
            ActivityCompat.requestPermissions(Objects.requireNonNull(getActivity()), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 123);
        }

        if (ContextCompat.checkSelfPermission(Objects.requireNonNull(getContext()), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_DENIED) {
            ActivityCompat.requestPermissions(Objects.requireNonNull(getActivity()), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 101);
        }

        binding.btnDownloadReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                generatePDF();
            }
        });

        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        NmapDetailsViewModel.Factory factory = new NmapDetailsViewModel.Factory(
                Objects.requireNonNull(getActivity()).getApplication(), target_id, output_nmap_id);
        viewModel = ViewModelProviders.of(this, factory).get(NmapDetailsViewModel.class);
        observeViewModel(viewModel);
    }

    public void observeViewModel(NmapDetailsViewModel viewModel) {
        viewModel.getNmapListDetails().observe(this, details -> {
            if (details != null) {
                adapter.setNetworkScanningAssessments(details);
                adapter.notifyDataSetChanged();
            }

            binding.tvTargetIp.setText("Target IP Address: " + details.get(0).getIp_address());
            binding.tvTargetHostname.setText("Target Hostname: " + details.get(0).getHostname());
            binding.tvTargetDate.setText("Scanned On: " + details.get(0).getDate());
            binding.tvServerProduct.setText("Server Product: " + details.get(0).getS_product());
            binding.tvServerVersion.setText("Server Version: " + details.get(0).getS_version());
            binding.tvServerInfo.setText("Server Information: " + details.get(0).getS_info());
            binding.tvServerTunnel.setText("Server Tunnel: " + details.get(0).getS_tunnel());
            binding.tvServerMethod.setText("Server Method: " + details.get(0).getS_method());
            binding.tvServerConf.setText("Server Configuration: " + details.get(0).getS_conf());
            binding.tvAssessmentName.setText(details.get(0).getAssessment_name());
            binding.tvPort.setText("Port: " + details.get(0).getPortid());
            setPortDesc(details.get(0).getPortid());
            if (!details.get(1).getPortid().isEmpty()) {
                binding.tvPort2.setText("Port: " + details.get(1).getPortid());
                setPortDesc2(details.get(1).getPortid());
            }
        });
    }

    private void setPortDesc(String port) {
        if (port.equals(PORT_21)) {
            binding.tvPortShortDesc.setText(PORT_21_TITLE);
        }
        if (port.equals(PORT_22)) {
            binding.tvPortShortDesc.setText(PORT_22_TITLE);
        }
        if (port.equals(PORT_23)) {
            binding.tvPortShortDesc.setText(PORT_23_TITLE);
        }
        if (port.equals(PORT_25)) {
            binding.tvPortShortDesc.setText(PORT_25_TITLE);
        }
        if (port.equals(PORT_80)) {
            binding.tvPortShortDesc.setText(PORT_80_TITLE);
        }
        if (port.equals(PORT_110)) {
            binding.tvPortShortDesc.setText(PORT_110_TITLE);
        }
        if (port.equals(PORT_143)) {
            binding.tvPortShortDesc.setText(PORT_143_TITLE);
        }
        if (port.equals(PORT_443)) {
            binding.tvPortShortDesc.setText(PORT_443_TITLE);
        }
        if (port.equals(PORT_445)) {
            binding.tvPortShortDesc.setText(PORT_445_TITLE);
        }
        if (port.equals(PORT_3389)) {
            binding.tvPortShortDesc.setText(PORT_3389_TITLE);
        }
    }

    private void setPortDesc2(String port) {
        if (port.equals(PORT_21)) {
            binding.tvPort2ShortDesc.setText(PORT_21_TITLE);
        }
        if (port.equals(PORT_22)) {
            binding.tvPort2ShortDesc.setText(PORT_22_TITLE);
        }
        if (port.equals(PORT_23)) {
            binding.tvPort2ShortDesc.setText(PORT_23_TITLE);
        }
        if (port.equals(PORT_25)) {
            binding.tvPort2ShortDesc.setText(PORT_25_TITLE);
        }
        if (port.equals(PORT_80)) {
            binding.tvPort2ShortDesc.setText(PORT_80_TITLE);
        }
        if (port.equals(PORT_110)) {
            binding.tvPort2ShortDesc.setText(PORT_110_TITLE);
        }
        if (port.equals(PORT_143)) {
            binding.tvPort2ShortDesc.setText(PORT_143_TITLE);
        }
        if (port.equals(PORT_443)) {
            binding.tvPort2ShortDesc.setText(PORT_443_TITLE);
        }
        if (port.equals(PORT_445)) {
            binding.tvPort2ShortDesc.setText(PORT_445_TITLE);
        }
        if (port.equals(PORT_3389)) {
            binding.tvPort2ShortDesc.setText(PORT_3389_TITLE);
        }
    }

    public boolean generatePDF() {
        Document document = new Document();
        try {
            //Create file path for Pdf
            String path = "/sdcard/";
            File dirFile = new File(path);
            if (!dirFile.exists()) {
                dirFile.createNewFile();
            }

            Log.d("PDFCreator", "PDF Path: " + path);

            String fileName = "/target_id_" + target_id + ".pdf";
            File file = new File(getActivity().getExternalFilesDir("report"), fileName);

            // create an instance of itext document
            PdfWriter.getInstance(document,
                    new FileOutputStream(file.getAbsoluteFile()));
            document.open();

            /* Create Paragraph and Set Font */
            Paragraph title_paragraph = new Paragraph(String.valueOf(binding.tvAssessmentName.getText()));
            Font paraFont = new Font(Font.FontFamily.TIMES_ROMAN);
            paraFont.setSize(20);
            paraFont.isBold();
            title_paragraph.setAlignment(Paragraph.ALIGN_CENTER);
            title_paragraph.setFont(paraFont);
            document.add(title_paragraph);

            Paragraph scanned_date_paragraph = new Paragraph(String.valueOf(binding.tvTargetDate.getText()));
            Font paraFont3 = new Font(Font.FontFamily.TIMES_ROMAN);
            paraFont3.setSize(12);
            scanned_date_paragraph.setAlignment(Paragraph.ALIGN_LEFT);
            scanned_date_paragraph.setFont(paraFont);
            document.add(scanned_date_paragraph);

            Paragraph ip_address_paragraph = new Paragraph(String.valueOf(binding.tvTargetIp.getText()));
            Font ip_address_font = new Font(Font.FontFamily.TIMES_ROMAN);
            ip_address_font.setSize(12);
            ip_address_paragraph.setAlignment(Paragraph.ALIGN_LEFT);
            ip_address_paragraph.setFont(paraFont);
            document.add(ip_address_paragraph);

            Paragraph target_paragraph = new Paragraph(String.valueOf(binding.tvTargetHostname.getText()));
            Font paraFont2 = new Font(Font.FontFamily.TIMES_ROMAN);
            paraFont2.setSize(12);
            target_paragraph.setAlignment(Paragraph.ALIGN_LEFT);
            target_paragraph.setFont(paraFont);
            document.add(target_paragraph);

            Paragraph server_details_paragraph = new Paragraph("Server Details:");
            Font server_details_font = new Font(Font.FontFamily.TIMES_ROMAN);
            server_details_font.setSize(12);
            server_details_paragraph.setAlignment(Paragraph.ALIGN_LEFT);
            server_details_paragraph.setFont(paraFont);
            document.add(server_details_paragraph);

            Paragraph server_product_paragraph = new Paragraph(String.valueOf(binding.tvServerProduct.getText()));
            Font server_product_font = new Font(Font.FontFamily.TIMES_ROMAN);
            server_product_font.setSize(12);
            server_product_paragraph.setAlignment(Paragraph.ALIGN_LEFT);
            server_product_paragraph.setFont(paraFont);
            document.add(server_product_paragraph);

            Paragraph server_version_paragraph = new Paragraph(String.valueOf(binding.tvServerVersion.getText()));
            Font server_version_font = new Font(Font.FontFamily.TIMES_ROMAN);
            server_version_font.setSize(12);
            server_version_paragraph.setAlignment(Paragraph.ALIGN_LEFT);
            server_version_paragraph.setFont(paraFont);
            document.add(server_version_paragraph);

            Paragraph server_information_paragraph = new Paragraph(String.valueOf(binding.tvServerInfo.getText()));
            Font server_information_font = new Font(Font.FontFamily.TIMES_ROMAN);
            server_information_font.setSize(12);
            server_information_paragraph.setAlignment(Paragraph.ALIGN_LEFT);
            server_information_paragraph.setFont(paraFont);
            document.add(server_information_paragraph);

            Paragraph server_tunnel_paragraph = new Paragraph(String.valueOf(binding.tvServerTunnel.getText()));
            Font server_tunnel_font = new Font(Font.FontFamily.TIMES_ROMAN);
            server_tunnel_font.setSize(12);
            server_tunnel_paragraph.setAlignment(Paragraph.ALIGN_LEFT);
            server_tunnel_paragraph.setFont(paraFont);
            document.add(server_tunnel_paragraph);

            Paragraph server_method_paragraph = new Paragraph(String.valueOf(binding.tvServerMethod.getText()));
            Font server_method_font = new Font(Font.FontFamily.TIMES_ROMAN);
            server_method_font.setSize(12);
            server_method_paragraph.setAlignment(Paragraph.ALIGN_LEFT);
            server_method_paragraph.setFont(paraFont);
            document.add(server_method_paragraph);

            Paragraph server_configuration_paragraph = new Paragraph(String.valueOf(binding.tvServerConf.getText()));
            Font server_configuration_font = new Font(Font.FontFamily.TIMES_ROMAN);
            server_configuration_font.setSize(12);
            server_configuration_paragraph.setAlignment(Paragraph.ALIGN_LEFT);
            server_configuration_paragraph.setFont(paraFont);
            document.add(server_configuration_paragraph);

            @SuppressLint("SimpleDateFormat")
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy hh-mm-ss");
            String format = simpleDateFormat.format(new Date());
            Paragraph generated_on_paragraph = new Paragraph("Generated on: " + format);
            Font paraFont4 = new Font(Font.FontFamily.TIMES_ROMAN);
            paraFont4.setSize(10);
            generated_on_paragraph.setAlignment(Paragraph.ALIGN_LEFT);
            generated_on_paragraph.setFont(paraFont);
            document.add(generated_on_paragraph);

            Paragraph footer = new Paragraph("Report generated by: VATool");
            Font paraFont5 = new Font(Font.FontFamily.TIMES_ROMAN);
            paraFont5.setSize(10);
            footer.setAlignment(Paragraph.ALIGN_BOTTOM);
            footer.setFont(paraFont);
            document.add(footer);

            AlertDialog.Builder builder = new AlertDialog.Builder(Objects.requireNonNull(getContext()), R.style.AlertDialogTheme);
            builder.setMessage("Do you want to open the file?");
            builder.setPositiveButton("Open", ((dialog, which) -> {
                openPDF();
            }));
            builder.setNegativeButton(android.R.string.cancel, (dialog, id) -> dialog.cancel());
            AlertDialog alertDialog = builder.create();
            alertDialog.show();

            // close document
            document.close();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        } catch (DocumentException e) {
            e.printStackTrace();
            return false;
        }
    }

    public void openPDF() {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        String path = "/sdcard/";
        String fileName = "/target_id_" + target_id + ".pdf";
        File file = new File(getActivity().getExternalFilesDir("report"), fileName);

        intent.setDataAndType(Uri.fromFile(file), "application/pdf");
        try {
            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(getContext(), "No Application available to view pdf", Toast.LENGTH_LONG).show();
        }
    }
}
