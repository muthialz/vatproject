package com.ddrzl.vatproject.dashboard.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.ddrzl.vatproject.R;
import com.ddrzl.vatproject.dashboard.entity.NetworkScanningAssessment;
import com.ddrzl.vatproject.dashboard.listener.OnClickCallback;
import com.ddrzl.vatproject.databinding.ItemAssessmentHistoryBinding;

import java.util.List;
import java.util.Objects;

public class NmapHistoryAdapter extends RecyclerView.Adapter<NmapHistoryAdapter.ViewHolder> {
    private List<? extends NetworkScanningAssessment> network_scanning_histories;
    private ItemAssessmentHistoryBinding binding;

    public void setNetwork_scanning_histories(List<? extends NetworkScanningAssessment> network_scanning_histories) {
        this.network_scanning_histories = network_scanning_histories;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public NmapHistoryAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = (LayoutInflater) viewGroup.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        binding = DataBindingUtil.inflate(Objects.requireNonNull(inflater), R.layout.item_assessment_history, viewGroup, false);
        binding.setCallback(new OnClickCallback());
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull NmapHistoryAdapter.ViewHolder viewHolder, int i) {
        viewHolder.binding.setHistory(network_scanning_histories.get(i));
        viewHolder.binding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return (network_scanning_histories == null ? 0 : network_scanning_histories.size());
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        final ItemAssessmentHistoryBinding binding;

        ViewHolder(ItemAssessmentHistoryBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
