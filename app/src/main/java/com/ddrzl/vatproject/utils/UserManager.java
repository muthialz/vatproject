package com.ddrzl.vatproject.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

import com.ddrzl.vatproject.login.entity.User;

import java.lang.ref.WeakReference;

public class UserManager {
    private static final String PREFER_NAME = "UserSessionPreference";
    private static final String IS_USER_LOGIN = "IsUserLoggedIn";
    private static final String KEY_ID = "User ID";
    private static final String KEY_EMAIL = "Email";
    private static final String KEY_USERNAME = "Username";
    private static final String KEY_FULL_NAME = "Full Name";
    private static final String KEY_REMEMBER_USER = "rememberUser";
    private static UserManager userManager;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    @SuppressLint("CommitPrefEdits")
    private UserManager(Context c) {
        WeakReference<Context> context = new WeakReference<>(c);
        preferences = context.get().getSharedPreferences(PREFER_NAME, 0);
        editor = preferences.edit();
    }

    public synchronized static UserManager getInstance(Context c) {
        if (userManager == null) {
            userManager = new UserManager(c);
        }
        return userManager;
    }

    public void createUserLoginSession(User user) {
        editor.putBoolean(IS_USER_LOGIN, true);
        editor.putInt(KEY_ID, user.getUser_id());
        editor.putString(KEY_EMAIL, user.getEmail());
        editor.putString(KEY_FULL_NAME, user.getFull_name());

        editor.commit();
    }

    public void rememberUserSession() {
        editor.putBoolean(IS_USER_LOGIN, false);
        editor.putInt(KEY_ID, 0);
        editor.putString(KEY_EMAIL, null);
        editor.putString(KEY_FULL_NAME, null);

        editor.commit();
    }

    public void updateUserDetailInteger(String key, int detail) {
        editor.putInt(key, detail);
        editor.commit();
    }

    public void updateUserDetailString(String key, String detail) {
        editor.putString(key, detail);
        editor.commit();
    }

    public boolean isNotLoggedIn() {
        return !preferences.getBoolean(IS_USER_LOGIN, false);
    }

    public String getRememberUsername() {
        return preferences.getString(KEY_REMEMBER_USER, "");
    }

    public void cancelLogin() {
        if (userManager != null) {
            editor.putString(KEY_REMEMBER_USER, preferences.getString(KEY_EMAIL, ""));
            editor.commit();
            rememberUserSession();
        }
    }

    public void logoutUser() {
        cancelLogin();
    }

    public User getUser() {
        User user = new User();
        user.setUser_id(preferences.getInt(KEY_ID, 0));
        user.setEmail(preferences.getString(KEY_EMAIL, null));
        user.setUsername(preferences.getString(KEY_USERNAME, null));
        user.setFull_name(preferences.getString(KEY_FULL_NAME, null));

        return user;
    }
}
