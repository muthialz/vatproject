package com.ddrzl.vatproject.utils;

import android.widget.EditText;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import static com.ddrzl.vatproject.utils.ConstantUtil.df;

public class DateUtil {
    private static DateFormat dateTimeJSONFormat;
    private static DateFormat dateTimeDBFormat;
    private static DateFormat viewDateTimeFormat;
    private static DateFormat viewDateTimeFormat2;

    static {
        dateTimeDBFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        viewDateTimeFormat = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH);
        dateTimeJSONFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH);
        viewDateTimeFormat2 = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss", Locale.ENGLISH);
    }

    public static String getDBDate() {
        return dateTimeDBFormat.format(new Date());
    }

    public static String convertViewFormatToDB(EditText editText) {
        return convertViewFormatToDB(editText.getText().toString());
    }

    private static String convertViewFormatToDB(String dateViewInput) {
        try {
            Date date = viewDateTimeFormat.parse(dateViewInput);
            return dateTimeDBFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String convertDBFormatToView(String dateDB) {
        try {
            Date date = dateTimeDBFormat.parse(dateDB);
            return viewDateTimeFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String convertDBFormatToView2(String dateDB) {
        try {
            Date date = dateTimeDBFormat.parse(dateDB);
            return viewDateTimeFormat2.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String setDateString(int day, int month, int year) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, day);
        return viewDateTimeFormat.format(calendar.getTime());
    }

    public static String cJSON(String jsonDate) throws ParseException {
        Date date;
        if (jsonDate == null)
            return null;
        if (jsonDate.isEmpty())
            return null;
        if (jsonDate.equalsIgnoreCase("null"))
            return null;

        return dateTimeDBFormat.format(dateTimeDBFormat.parse(jsonDate));
    }

    public static String getCurrentTimeStamp() {
        try {
            return dateTimeDBFormat.format(new Date()); // Find todays date
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String getDay(String dateDB) {
        try {
            Date date = dateTimeDBFormat.parse(dateDB);
            return new SimpleDateFormat("EEEE, dd MMM yyyy, HH:mm", Locale.ENGLISH).format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static boolean isAfter(String start_date, String from_date) {
        try {
            Date start = df.parse(start_date);
            Date from = df.parse(from_date);
            return from.after(start) || from.equals(start);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return false;
    }
}
