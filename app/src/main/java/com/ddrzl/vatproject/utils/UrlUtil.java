package com.ddrzl.vatproject.utils;

public class UrlUtil {
    public static final String BASE_URL = "http://vat.makankor.net/vat-restful-api/codeigniter/framework/index.php/api/";
    public static final String URL_LOGIN = "authentication/login";
    public static final String URL_REGISTRATION = "authentication/registration";
    public static final String URL_CREATE_ASSESSMENT = "Assessment/target";
    public static final String URL_GET_ASSESSMENT_HISTORY = "history/user";
    public static final String URL_GET_TARGET_LIST = "assessment/targetlist";
    public static final String URL_SCAN_NMAP = "nmap/scan/";
    public static final String URL_GET_NMAP_DETAILS = "history/target/";
    public static final String URL_SCAN_NIKTO = "nikto/scan/";
    public static final String URL_GET_NIKTO_DETAILS = "history/targetnikto";
    public static final String URL_UPDATE_ASSESSMENT = "Assessment/update";
}