package com.ddrzl.vatproject.utils;

import com.ddrzl.vatproject.annotation.DateTimeString;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.text.ParseException;

public class JSONUtil {
    public <T> T toObj(T obj, JSONObject json) throws JSONException, IllegalAccessException, ParseException {
        T tempObj = (T) obj;
        Field[] fields = obj.getClass().getDeclaredFields();
        for (Field field : fields) {
            String columnName = field.getName();
            if (json.isNull(columnName))
                continue;

            field.setAccessible(true);
            Class<?> c = field.getType();
            if (c == String.class) {
                if (field.isAnnotationPresent(DateTimeString.class))
                    field.set(tempObj, DateUtil.cJSON(json.getString(columnName)));
                else
                    field.set(tempObj, json.getString(columnName));
            } else if (c == int.class) {
                field.set(tempObj, json.getInt(columnName));
            } else if (c == double.class) {
                field.set(tempObj, json.getDouble(columnName));
            } else if (c == boolean.class) {
                field.set(tempObj, json.getInt(columnName) != 0);
            }
        }
        return tempObj;
    }
}
