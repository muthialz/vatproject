package com.ddrzl.vatproject.utils;

import java.text.SimpleDateFormat;
import java.util.Locale;

public class ConstantUtil {
    public static final String EMAIL_DEMO = "muthia@gmail.com";
    public static final String PASSWORD_DEMO = "12345";
    public static final int STORAGE_PERMISSION_CODE = 123;
    public final static SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
    public static final String[] ASSESSMENT_TOOLS = {
            "Select Assessment Tool", "Network Scanning", "Web Server Scanning"
    };
    public static final int NETWORK_SCANNING = 1;
    public static final int WEB_SCANNING = 2;
    public static final String VULNERABILITY_IGNORED_1 = "/home/: This might be interesting...";
    public static final String VULNERABILITY_IGNORED_2 = "/login/: This might be interesting...";
    public static final String PORT_21 = "21";
    public static final String PORT_21_TITLE = "This port is used for File Transfer (FTP)";
    public static final String PORT_22 = "22";
    public static final String PORT_22_TITLE = "This port is used for Secure Shell (SSH)";
    public static final String PORT_23 = "23";
    public static final String PORT_23_TITLE = "This port is used for Secure Shell (SSH)";
    public static final String PORT_25 = "25";
    public static final String PORT_25_TITLE = "This port is used for Mail (SMTP)";
    public static final String PORT_80 = "80";
    public static final String PORT_80_TITLE = "This port is used for Web (HTTP)";
    public static final String PORT_110 = "110";
    public static final String PORT_110_TITLE = "This port is used for Mail (POP3)";
    public static final String PORT_143 = "143";
    public static final String PORT_143_TITLE = "This port is used for Mail (IMAP)";
    public static final String PORT_443 = "443";
    public static final String PORT_443_TITLE = "This port is used for SSL/TLS (HTTPS)";
    public static final String PORT_445 = "445";
    public static final String PORT_445_TITLE = "This port is used for Microsoft (SMB)";
    public static final String PORT_3389 = "445";
    public static final String PORT_3389_TITLE = "This port is used for Microsoft (SMB)";
}
