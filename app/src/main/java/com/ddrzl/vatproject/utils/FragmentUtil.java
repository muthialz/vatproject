package com.ddrzl.vatproject.utils;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;

import com.ddrzl.vatproject.R;
import com.ddrzl.vatproject.dashboard.activity.MainActivity;

public class FragmentUtil {
    public static void loadFragment(FragmentActivity context, Fragment fragment, Boolean addToBackStack) {
        FragmentTransaction transaction = context.getSupportFragmentManager().beginTransaction();
        if (context instanceof MainActivity) {
            transaction.replace(R.id.frame_layout_main, fragment, "TARGET_TAG");
        }
        if (addToBackStack) {
            transaction.addToBackStack(fragment.getClass().getName());
        }
        transaction.commitAllowingStateLoss();
    }
}
