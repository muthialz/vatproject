package com.ddrzl.vatproject.login;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.ddrzl.vatproject.R;
import com.ddrzl.vatproject.dashboard.activity.MainActivity;
import com.ddrzl.vatproject.login.entity.User;
import com.ddrzl.vatproject.utils.HTTPUtil;
import com.ddrzl.vatproject.utils.JSONUtil;
import com.ddrzl.vatproject.utils.UserManager;
import com.ddrzl.vatproject.volley.VolleyCallback;
import com.ddrzl.vatproject.volley.VolleyRequest;
import com.github.jlmd.animatedcircleloadingview.AnimatedCircleLoadingView;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;

import static com.ddrzl.vatproject.utils.UrlUtil.BASE_URL;
import static com.ddrzl.vatproject.utils.UrlUtil.URL_LOGIN;
import static com.ddrzl.vatproject.utils.UrlUtil.URL_REGISTRATION;

public class LoginActivity extends AppCompatActivity {
    private AnimatedCircleLoadingView animatedCircleLoadingView;
    private EditText mEmail, mPassword;
    private EditText et_email, et_username, et_full_name, et_password;
    private TextView tv_signUp, tv_forgot_pass;
    private Button btn_signIn;
    private String email, password;
    private UserManager userManager;
    private User user = new User();
    private View viewLoginForm;
    private JSONUtil jsonUtil = new JSONUtil();
    private Boolean closeDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_login);

        userManager = UserManager.getInstance(getApplicationContext());

        mEmail = findViewById(R.id.et_email);
        mPassword = findViewById(R.id.et_password);
        btn_signIn = findViewById(R.id.btn_sign_in);
        tv_signUp = findViewById(R.id.txt_signUp);
        tv_forgot_pass = findViewById(R.id.txt_forgotPass);
        viewLoginForm = findViewById(R.id.login_screen_form);

        btn_signIn.setOnClickListener(v -> {
            validateLogin();
        });

        tv_signUp.setOnClickListener(v -> {
            createAccount();
        });

        tv_forgot_pass.setOnClickListener(v -> {
            forgotPassword();
        });

        animatedCircleLoadingView = (AnimatedCircleLoadingView) findViewById(R.id.circle_loading_view);
    }

    private void startLoading() {
        animatedCircleLoadingView.startDeterminate();
        animatedCircleLoadingView.setPercent(100);
    }

    private void startPercentMockThread() {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1500);
                    for (int i = 0; i <= 100; i++) {
                        Thread.sleep(65);
                        changePercent(i);
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        new Thread(runnable).start();
    }

    private void changePercent(final int percent) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                animatedCircleLoadingView.setPercent(percent);
            }
        });
    }

    public void resetLoading() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                animatedCircleLoadingView.resetLoading();
            }
        });
    }

    private void showProgress(final boolean show) {
        if (show) {
            viewLoginForm.setVisibility(View.GONE);
            startLoading();
            startPercentMockThread();
        } else {
            viewLoginForm.setVisibility(View.VISIBLE);
            animatedCircleLoadingView.setVisibility(View.GONE);
        }
    }

    public void validateLogin() {
        email = mEmail.getText().toString();
        password = mPassword.getText().toString();

        boolean cancel = false;
        View focusView = null;

        if (!HTTPUtil.isNetworkAvailable(this)) {
            Toast.makeText(LoginActivity.this, R.string.no_internet_connection, Toast.LENGTH_SHORT).show();
            cancel = true;
        }

        if (TextUtils.isEmpty(password)) {
            mPassword.setError(getString(R.string.error_field_required));
            focusView = mPassword;
            cancel = true;
        }

        if (TextUtils.isEmpty(email)) {
            mEmail.setError(getString(R.string.error_field_required));
            focusView = mEmail;
            cancel = true;
        }

        if (cancel) {
            focusView.requestFocus();
        }
        else {
            mPassword.clearFocus();
            mEmail.clearFocus();
            InputMethodManager in = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
            assert in != null;
            in.hideSoftInputFromWindow(mEmail.getWindowToken(), 0);
            in.hideSoftInputFromWindow(mPassword.getWindowToken(), 0);
            showProgress(true);
            getUser(email, password);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            finishAffinity();
        }
    }

    public void createAccount() {
        LayoutInflater layoutInflater = getLayoutInflater();
        View infoDialogLayout = layoutInflater.inflate(R.layout.dialog_sign_up, null);
        et_email = infoDialogLayout.findViewById(R.id.et_input_email);
        et_full_name = infoDialogLayout.findViewById(R.id.et_input_full_name);
        et_password = infoDialogLayout.findViewById(R.id.et_password);

        AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
        builder.setView(infoDialogLayout)
                .setTitle("Sign Up")
                .setPositiveButton("Register Account", ((dialog, which) -> {
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //Do nothing here because we override this button later to change the close behaviour.
                            //However, we still need this because on older versions of Android unless we
                            //pass a handler the button doesn't get instantiated
                        }
                    };
                }));
        final AlertDialog dialog = builder.create();
        dialog.show();
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Boolean closeDialog = false;
                validateRegisterAccount(false);
                if (closeDialog)
                    dialog.dismiss();
                //else dialog stays open. Make sure you have an obvious way to close the dialog especially if you set cancellable to false.
            }
        });
    }

    public void validateRegisterAccount(Boolean closeDialog) {
        String full_name = et_full_name.getText().toString();
        String email = et_email.getText().toString() + "@vat.net";
        String password = et_password.getText().toString();

        boolean cancel = false;
        View focusView = null;

        if (!HTTPUtil.isNetworkAvailable(this)) {
            Toast.makeText(LoginActivity.this, R.string.no_internet_connection, Toast.LENGTH_SHORT).show();
            cancel = true;
        }

        if (TextUtils.isEmpty(password)) {
            et_password.setError(getString(R.string.error_field_required));
            focusView = et_password;
            cancel = true;
        }

        if (TextUtils.isEmpty(email)) {
            et_email.setError(getString(R.string.error_field_required));
            focusView = et_email;
            cancel = true;
        }

        if (TextUtils.isEmpty(full_name)) {
            et_full_name.setError(getString(R.string.error_field_required));
            focusView = et_full_name;
            cancel = true;
        }

        if (cancel) {
            focusView.requestFocus();
            closeDialog = false;
        } else {
            closeDialog = true;
            et_full_name.clearFocus();
            et_email.clearFocus();
            et_password.clearFocus();
            InputMethodManager in = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
            assert in != null;
            in.hideSoftInputFromWindow(et_full_name.getWindowToken(), 0);
            in.hideSoftInputFromWindow(et_email.getWindowToken(), 0);
            in.hideSoftInputFromWindow(et_password.getWindowToken(), 0);
        }
    }

    private void registerAccount(String full_name, String email, String password) {
        VolleyRequest request = new VolleyRequest();
        String url = BASE_URL + URL_REGISTRATION;
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("full_name", full_name);
            jsonObject.put("email", email);
            jsonObject.put("password", password);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        request.postJSONObjectRequest(getApplicationContext(), url, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccessResponse(String result) {
                try {
                    JSONObject jsonResult = new JSONObject(result);
                    if (jsonResult.getBoolean("status")) {
                        Toast.makeText(getApplicationContext(), "Account registered successfully", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getApplicationContext(), "Failed to register account", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "Failed to register account", Toast.LENGTH_LONG).show();
                Log.e("ERROR", "Error occurred ", error);
            }
        });
    }

    public void forgotPassword() {
        LayoutInflater layoutInflater = getLayoutInflater();
        View infoDialogLayout = layoutInflater.inflate(R.layout.dialog_forgot_password, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(infoDialogLayout)
                .setTitle("Forgot Password")
                .setPositiveButton("Change Password", ((dialog, which) -> {
                    Toast.makeText(getApplicationContext(), "Password changed. Please sign in with your new password", Toast.LENGTH_SHORT).show();
                }
                ));
        builder.show();
    }

    private void getUser(String email, String password) {
        VolleyRequest request = new VolleyRequest();
        String url = BASE_URL + URL_LOGIN;
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("email", email);
            jsonObject.put("password", password);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        request.postJSONObjectRequest(getApplicationContext(), url, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccessResponse(String result) {
                try {
                    JSONObject jsonResult = new JSONObject(result);
                    if (jsonResult.getBoolean("status")) {
                        user = new User();
                        jsonUtil.toObj(user, jsonResult.getJSONObject("data"));
                        handleUserAfterLogin(user);
                        animatedCircleLoadingView.stopOk();
                    } else {
                        userManager.cancelLogin();
                        showProgress(false);
                        Toast.makeText(getApplicationContext(), "Email or Password is incorrect", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorResponse(VolleyError error) {
                userManager.cancelLogin();
                showProgress(false);
                Toast.makeText(getApplicationContext(), "Failed to login", Toast.LENGTH_LONG).show();
                Log.e("ERROR", "Error occurred ", error);
            }
        });
    }

    private void handleUserAfterLogin(User user) {
        if (userManager.isNotLoggedIn()) {
            userManager.createUserLoginSession(user);
        } else {
            mEmail.setText(userManager.getRememberUsername());
        }
        Intent intent;
        intent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intent);
        finish();
    }
}
