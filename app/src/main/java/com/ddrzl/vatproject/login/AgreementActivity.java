package com.ddrzl.vatproject.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.widget.TextView;

import com.ddrzl.vatproject.BuildConfig;
import com.ddrzl.vatproject.R;

public class AgreementActivity extends AppCompatActivity {
    private String versionCode;
    private TextView agree, version;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_agreement);

        version = findViewById(R.id.tv_version);
        versionCode = BuildConfig.VERSION_NAME;
        version.setText("Version " + versionCode);

        agree = findViewById(R.id.tv_agree);
        agree.setOnClickListener(v -> {
            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(intent);
            finish();
        });
    }
}
